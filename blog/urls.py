from django.urls import path,include
from . import views
from django.views.i18n import JavaScriptCatalog

urlpatterns = [
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    path('', views.home, name='home'),
    path('modal/', views.modal, name='modal'),
    path('variable-modal/', views.variable_modal, name='variable-modal'),

    path('image-delete', views.imageDelete, name='image-delete'),
    path('sort-images', views.imageSort, name='sort-images'),
]



