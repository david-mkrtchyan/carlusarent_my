from django.shortcuts import render
from models.functions import get_variable
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from models.models import Images
from django.contrib.auth.models import User
import json
from django.views.decorators.csrf import csrf_exempt
from car.models import UserCars
from datetime import date


# Create your views here.

def home(request):
    today = date.today()
    #end_date__gt=today,
    usecars = UserCars.objects.filter(admin_confirmed=True).order_by('-id')[:6]

    return render(request, 'blog/home.html', {
        'usecars': usecars
    })


def modal(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        content = request.POST.get('content')
        type = request.POST.get('type')
        if type in ['basic', 'info']:
            html = render_to_string(request, f'modal/{type}.html', {
                'title': title,
                'content': content,
            })
            return JsonResponse({'success': True, 'html': html}, safe=False)


def variable_modal(request):
    if request.method == 'POST':
        key = request.POST.get('key')
        className = request.POST.get('class')
        model = get_variable(key, True)
        if model:
            html = render_to_string('modal/info.html', {
                'title': model.title,
                'content': model.value,
                'className': className,
            })
            return JsonResponse({'success': True, 'html': html}, safe=False)
    return JsonResponse({'success': True}, safe=False)


def bad_request(request):
    context = {}
    return render(request, 'errors/400.html', context, status=400)


def permission_denied(request):
    context = {}
    return render(request, 'errors/403.html', context, status=403)


def page_not_found(request):
    context = {}
    return render(request, 'errors/404.html', context, status=404)


def server_error(request):
    context = {}
    return render(request, 'errors/500.html', context, status=500)


@login_required
def imageSort(request):
    stacks = request.POST.get('stack')
    if request.method == 'POST' and stacks:
        jsonData = json.loads(stacks)
        for key, value in enumerate(jsonData):
            Images.objects.filter(id=int(value['key'])).update(rank=int(key + 1))

    return JsonResponse({'sucess': True})


@login_required
@csrf_exempt
def imageDelete(request):
    id = request.POST.get('key')
    car_id = request.POST.get('car_id')
    if request.method == 'POST' and id and car_id:
        car = UserCars.objects.filter(id=car_id, user_id=request.user.id).first()
        image = Images.objects.filter(id=int(id)).first()
        if car and image:
            Images.objects.filter(id=int(id)).delete()
            return JsonResponse({'sucess': True})
    return JsonResponse({'sucess': False})
