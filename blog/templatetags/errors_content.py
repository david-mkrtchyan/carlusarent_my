from django import template

register = template.Library()


@register.inclusion_tag('errors/content.html')
def content_errors(errors, field):
    return {'errors': errors, 'field': field}
