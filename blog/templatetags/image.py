from django import template
import os
from models.models import type_profile, type_car
from carlusarent.settings import BASE_DIR

register = template.Library()


@register.simple_tag()
def image_url(image=None, type=type_car, size='big', *args, **kwargs):
    media = 'media/uploads/'

    if image:
        imageName = os.path.basename(image.path)
        mediapath = f'/{media}{size}/{type}/{imageName}'
        path = os.path.join(f'{BASE_DIR}/{media}/{size}/{type}/', imageName)

        if size == 'small':
            if not os.path.isfile(path):
                image_url(image, type, 'big', *args, **kwargs)

        if os.path.isfile(path):
            return mediapath

    if type == type_car:
        return f'/media/uploads/default_car.png'
    elif type == type_profile:
        return f'/media/profile.png'
