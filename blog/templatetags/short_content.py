from django import template

register = template.Library()

@register.simple_tag()
def cut_title(title, letter=100, *args, **kwargs):
    if len(title) and len(title) > letter:
        return title[0:letter]
    else:
        return title
