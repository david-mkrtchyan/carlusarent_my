from django import template

register = template.Library()
from models.models import Variable


@register.simple_tag()
def get_variable(key, object=False):
    varibale = Variable.objects.filter(key=key).first()
    if varibale:
        if object == True:
            return varibale
        return varibale.value

