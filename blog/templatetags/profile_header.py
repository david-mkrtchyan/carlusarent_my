from django import template

register = template.Library()


@register.inclusion_tag('templatetags/profile_header.html')
def header(**kwargs):
    return {'kwargs': kwargs}
