from django import template
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.filter
@stringfilter
def convert(value, convert_type='str', *args, **kwargs):
    if convert_type == 'int':
        return int(value)
    elif convert_type == 'obj':
         pass
    return str(value)
