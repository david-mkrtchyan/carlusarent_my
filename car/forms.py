from django import forms
from .car_functions import year_first_registration, millages, years, open
from .models import CarBrand, CarModel, Feature, Extra
from .models import (
    type_feature_seat, type_feature_door, type_feature_fuel, type_feature_transmission, type_feature_mileage
)
from django.utils.translation import gettext as _
from .models import UserCars, UserCarsExtra, UserCarsConditions
from models.models import Images
from datetime import datetime
from models.functions import get_variable


class Calculation(forms.Form):
    country = forms.CharField(label=_('Country'))
    year = forms.ChoiceField(label=_('Year'), required=True,
                             choices=[(i, i) for k, i in enumerate(years())])

    millage = forms.ChoiceField(label=_('Millage'), required=True,
                                choices=[(v, v) for i, v in enumerate(millages())])

    brand = forms.ChoiceField(label=_('Brand'), required=True,
                              choices=[(br.id, br.name) for br in CarBrand.objects.all()])
    model = forms.ChoiceField(label=_('Model'), required=True,
                              choices=[(mod.id, mod.name) for mod in CarModel.objects.all()])


class UserCarsConditionsForm(forms.ModelForm):
    from .car_functions import max_booking_notice_list, preparationTime

    rental_duration = forms.CharField(label=_('Rental Duration'), required=False)

    max_booking_notice = forms.ChoiceField(choices=[(str(k), k) for k, i in max_booking_notice_list().items()],
                                           required=False)
    preparation_time = forms.ChoiceField(choices=[(k, k) for k, i in preparationTime().items()], required=False)

    special_requirements = forms.CharField(required=False)

    def clean_rental_duration(self):
        rental_duration = self.cleaned_data.get("rental_duration")

        if rental_duration:
            start, end = rental_duration.split(';')
            if not start or not end:
                raise forms.ValidationError(
                    _('Rental Duration is wrong'),
                )
        return rental_duration

    class Meta:
        model = UserCarsConditions

        fields = []
        exclude = ("user_car", 'special_requirements', 'rental_duration', 'preparation_time',
                   'max_booking_notice')


class AddCarForm(forms.ModelForm):
    brand_id = forms.ChoiceField(label=_('Brand'), required=True,
                                 choices=[(br.id, br.name) for br in CarBrand.objects.all()])
    model_id = forms.ChoiceField(label=_('Model'), required=True,
                                 choices=[(mod.id, mod.name) for mod in CarModel.objects.all()])
    registration_country = forms.CharField(label=_('Registration Country'), required=False)
    year_of_first_register = forms.ChoiceField(label=_('Year'), required=True,
                                               choices=[(i, i) for k, i in enumerate(year_first_registration())])

    number_plate = forms.CharField(label=_('Number plate'), required=True)
    number_of_seats_id = forms.ChoiceField(label=_('Number of seats'), required=True,
                                           choices=[(f.id, f.name) for f in
                                                    Feature.objects.filter(status=True, type=type_feature_seat).all()])
    number_of_doors_id = forms.ChoiceField(label=_('Number of Doors'), required=True,
                                           choices=[(f.id, f.name) for f in
                                                    Feature.objects.filter(status=True, type=type_feature_door).all()])
    fuel_id = forms.ChoiceField(label=_('Fuel'), required=True,
                                choices=[(f.id, f.name) for f in
                                         Feature.objects.filter(status=True, type=type_feature_fuel).all()])
    transmission_id = forms.ChoiceField(label=_('Transmission'), required=True,
                                        choices=[(f.id, f.name) for f in
                                                 Feature.objects.filter(status=True,
                                                                        type=type_feature_transmission).all()])
    mileage_id = forms.ChoiceField(label=_('Mileage'), required=True,
                                   choices=[(f.id, f.name) for f in
                                            Feature.objects.filter(status=True, type=type_feature_mileage).all()])

    daily_price = forms.IntegerField(label=_('Daily Price'), required=True)

    description = forms.Textarea()

    parking_address = forms.CharField(label=_('Parking Address'), required=True)
    parking_address_validate = forms.BooleanField(label=_('Parking Address'), required=False, )

    phone_number = forms.CharField(label='Phone Number', max_length=45, required=True)
    validate_phone = forms.BooleanField(label=_('Phone Number'), required=True)
    drivy_open = forms.ChoiceField(label=_('Open'), required=False, choices=[(v, v) for i, v in enumerate(open())])
    two_days_percentage = forms.CharField(label=_('Two days percentage'), required=True)
    one_week_percentage = forms.CharField(label=_('One week percentage'), required=True)
    one_month_percentage = forms.CharField(label=_('One month percentage'), required=True)
    displayed_months = forms.CharField(label=_('Displayed Months'), required=False)

    def clean_two_days_percentage(self):
        two_days_percentage = int(self.cleaned_data.get("two_days_percentage"))
        minPercent = int(get_variable('daily_price_discount_2_days_minimum'))
        if two_days_percentage == None or two_days_percentage < minPercent:
            raise forms.ValidationError(
                _('Two days percentage is wrong'),
            )
        return two_days_percentage

    def clean_one_week_percentage(self):
        one_week_percentage = int(self.cleaned_data.get("one_week_percentage"))
        minPercent = int(get_variable('daily_price_discount_1_week_minimum'))
        if one_week_percentage == None or one_week_percentage < minPercent:
            raise forms.ValidationError(
                _('One week percentage is wrong'),
            )
        return one_week_percentage

    def clean_one_month_percentage(self):
        one_month_percentage = int(self.cleaned_data.get("one_month_percentage"))
        minPercent = int(get_variable('daily_price_discount_1_month_minimum'))
        if one_month_percentage == None or one_month_percentage < minPercent:
            raise forms.ValidationError(
                _('One month percentage is wrong'),
            )
        return one_month_percentage

    def clean_validate_phone(self):
        validate_phone = self.cleaned_data.get("validate_phone")

        if validate_phone == None or validate_phone == False:
            raise forms.ValidationError(
                'Phone number is wrong',
            )
        return validate_phone

    def clean_validate_parking_address(self):
        parking_address_validate = self.cleaned_data.get("parking_address_validate")

        if parking_address_validate == None or parking_address_validate == False:
            raise forms.ValidationError(
                'Parking Address is wrong',
            )
        return parking_address_validate

    class Meta:
        model = UserCars

        fields = ['registration_country', 'year_of_first_register', 'two_days_percentage',
                  'one_week_percentage', 'one_month_percentage',
                  'number_plate', 'daily_price', 'parking_address', 'phone_number',
                  'description', 'displayed_months']
        exclude = ("user", 'brand', 'model', 'number_of_seats', 'fuel', 'transmission', 'mileage', 'number_of_doors',)


class AddCarExtrasForm(forms.ModelForm):
    extra_id = forms.ModelMultipleChoiceField(
        label='Extras', required=True,
        queryset=Extra.objects.filter(status=True).all(),  # not optional, use .all() if unsure
        widget=forms.CheckboxSelectMultiple,
    )

    class Meta:
        model = UserCarsExtra
        fields = []
        exclude = ("user", "user_car",)


class AddCarImageForm(forms.ModelForm):
    files = forms.FileField(label='Images', required=True)

    class Meta:
        model = Images
        fields = []
        exclude = ("object_id", "type", "rank", "path",)


class UpdateCarImageForm(forms.ModelForm):
    files = forms.FileField(label='Images', required=False)

    class Meta:
        model = Images
        fields = []
        exclude = ("object_id", "type", "rank", "path",)
