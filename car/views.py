from django.contrib.auth.decorators import login_required
from decorators.own_decorator import checkLoggedInAndRequestUserSame
from .models import CarBrand, CarModel, PriceCalculation
from .car_functions import millages, years, year_first_registration, weeks, timeSlots, preparationTime, \
    max_booking_notice_list
from django.http import JsonResponse, HttpResponseRedirect
from django.core import serializers
import json
from .forms import Calculation
from django.template.loader import render_to_string
from models.functions import get_variable
from django_countries import countries
from users.models import DriverLicense
from .models import UserCarsFavorite, Feature, Extra, type_feature_seat, type_feature_door, type_feature_fuel, \
    type_feature_transmission, \
    type_feature_mileage, UserCarsConditions
import math
from .forms import AddCarForm, AddCarExtrasForm, AddCarImageForm, UpdateCarImageForm, UserCarsConditionsForm
from .models import UserCarsExtra, UserCarsTimeSlot, UserCars, UserCarAvailableDate
from models.models import type_car, Images
from django.contrib import messages
from django.utils.translation import gettext as _
from users.models import Users as User
from django.http import HttpResponseNotFound
from django.shortcuts import get_object_or_404, reverse
from django.core.paginator import Paginator
from car.templatetags.file_exists import file_exists
from datetime import datetime, date, timedelta
from django.shortcuts import redirect
from dateutil import relativedelta
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@login_required
@checkLoggedInAndRequestUserSame
def cars(request, id):
    userCars = UserCars.objects.filter(user_id=id, admin_confirmed=True).order_by('-id').all()
    userData = User.objects.filter(id=id).first()
    if not userData:
        return HttpResponseNotFound()

    return render(request, 'car/user_cars.html', {
        'userCars': userCars,
        'userData': userData,
    })


# Create your views here.
@login_required
def listFavorite(request):
    favorites = UserCarsFavorite.objects.filter(user_id=request.user.id, favorite=True).order_by('-id').all()

    return render(request, 'car/favorites.html', {
        'favorites': favorites,
    })


# Create your views here.
def carview(request, id):
    car = get_object_or_404(UserCars, id=id, admin_confirmed=True)

    driverLicense = DriverLicense.objects.filter(user_id=request.user.id).first()
    country = dict(countries)[driverLicense.issuing_country] if driverLicense else None

    return render(request, 'car/view-car.html', {
        'car': car,
        'country': country,
    })


@login_required
def addFavorite(request):
    id = request.POST.get('id')
    if id and request.is_ajax():

        userCars = UserCars.objects.filter(id=id, admin_confirmed=True).first()
        if userCars:
            favorite = UserCarsFavorite.objects.filter(user_id=request.user.id, user_car_id=id).first()
            if favorite:
                if favorite.favorite == False:
                    favorite.favorite = True
                    favorite.save()
                    return JsonResponse({'success': True, 'favorite': True}, status=200, safe=False)
                elif favorite.favorite == True:
                    favorite.favorite = False
                    favorite.save()
                    return JsonResponse({'success': True, 'favorite': False}, status=200, safe=False)
            else:
                UserCarsFavorite.objects.create(
                    user_id=request.user.id,
                    user_car_id=id,
                    favorite=True
                )
                return JsonResponse({'success': True, 'favorite': True}, status=200, safe=False)

    return JsonResponse({'success': False}, status=200, safe=False)


@login_required
@checkLoggedInAndRequestUserSame
def addCar(request, id):
    userData = User.objects.filter(id=id).first()

    brands = CarBrand.objects.all()
    return render(request, 'car/add.html', {
        'brands': brands,
        'years': years,
        'millages': millages,
        'userData': userData,
    })


@login_required
def deleteCar(request, id):
    car = get_object_or_404(UserCars, pk=id, user_id=request.user.id)
    UserCarsTimeSlot.objects.filter(user_car_id=car.id).all().delete()
    UserCarsExtra.objects.filter(user_car_id=car.id).all().delete()
    car.delete()
    return redirect('list-cars', request.user.id)


@login_required
def moreCalendar(request):
    from car.templatetags.calendar_booking import calendar_data
    post = request.POST
    year_month_day = post.get('year_month_day')
    displayedMonth= post.get('displayed_months')

    displayed_months = int(displayedMonth)-6 if displayedMonth and int(displayedMonth) != 6 and int(displayedMonth) % 2 == 0 else 6

    if year_month_day:
        addDate = datetime.strptime(request.POST.get('year_month_day'), '%Y-%m-%d')

        html = render_to_string('templatetags/calendar2.html', calendar_data(addDate, '', displayed_months))

        nextmonth = addDate + relativedelta.relativedelta(months=displayed_months)

        return JsonResponse({'success': True, 'html': html, 'nextmonth': nextmonth.strftime("%Y-%m-%d")}, safe=False)


@csrf_exempt
@login_required
def addCalendarDate(request):
    if request.is_ajax() and request.method == 'POST' and request.body:
        body = json.loads(request.body.decode('utf-8'))
        start_date = body.get('start_date')
        start_time = body.get('start_time')
        end_date = body.get('end_date')
        end_time = body.get('end_time')
        new_status = body.get('new_status')
        displayed_months = body.get('displayed_months')
        repeat = body.get('repeat')

        start = datetime.strptime(start_date, "%Y-%m-%d")
        end = datetime.strptime(end_date, "%Y-%m-%d")
        date_array = (start + timedelta(days=x) for x in range(0, (end + timedelta(days=1) - start).days))
        dateobj = {}
        if repeat:
            start_first = start.strftime('%Y-%m-%d')
            end_first = end.strftime('%Y-%m-%d')

            getdateobj(dateobj, date_array, start_first, start_time, end_first, end_time, new_status)

            for s in range(1, int(repeat)):
                start += relativedelta.relativedelta(days=7)
                end += relativedelta.relativedelta(days=7)

                date_array = (start + timedelta(days=x) for x in range(0, (end + timedelta(days=1) - start).days))

                start2 = start.strftime('%Y-%m-%d')
                end2 = end.strftime('%Y-%m-%d')

                getdateobj(dateobj, date_array, start2, start_time, end2, end_time, new_status)

            return JsonResponse({'repeat': dateobj, 'new_status': new_status}, safe=False)
        else:
            getdateobj(dateobj, date_array, start_date, start_time, end_date, end_time, new_status)
            return JsonResponse(dateobj, safe=False)


def getdateobj(dateobj, date_array, start_date, start_time, end_date, end_time, new_status):
    from car.templatetags.calendar_booking import getDates

    for date_object in date_array:

        weekday = date_object.strftime('%a')

        date = date_object.strftime("%Y-%m-%d")
        if date == start_date and start_time == 'pm':
            dateobj[date] = [
                {"date": date, "time": "pm", "weekDay": getDates()[weekday]}
                # {'time': 'pm', 'availability': new_status}
            ]
        elif date == end_date and end_time == 'am':
            dateobj[date] = [
                {"date": date, "time": "am", "weekDay": getDates()[weekday]}
                # {'time': 'am', 'availability': new_status},
            ]
        else:
            dateobj[date] = [
                {"date": date, "time": "am", "weekDay": getDates()[weekday]},
                {"date": date, "time": "pm", "weekDay": getDates()[weekday]},
                # {'time': 'am', 'availability': new_status},
                # {'time': 'pm', 'availability': new_status}
            ]
    return dateobj


@csrf_exempt
@login_required
def editCalendarDate(request):
    return JsonResponse({'success': True}, safe=False)


def viewCar(request, id):
    userCars_list = UserCars.objects.filter(user_id=id, admin_confirmed=True).order_by('-id').all()
    userData = get_object_or_404(User, pk=id)

    paginator = Paginator(userCars_list, 25)  # Show 25 contacts per page
    page = request.GET.get('page')
    userCars = paginator.get_page(page)

    return render(request, 'car/person_cars.html', {
        'userCars': userCars,
        'userData': userData,
    })


@login_required
@checkLoggedInAndRequestUserSame
def car_wizards_edit(request, id, car_id):
    carObj = get_object_or_404(UserCars, pk=car_id)

    imagesObject = Images.objects.filter(object_id=car_id, type=type_car).order_by('rank').all()

    if request.method == 'POST':
        form = AddCarForm(request.POST, instance=carObj)
        extra = AddCarExtrasForm(request.POST)
        images = UpdateCarImageForm(request.POST, request.FILES)
        condition = UserCarsConditionsForm(request.POST)

        post = request.POST
        errors_slot_all = []
        timeslotsData = []
        for key, week in weeks().items():
            type = post.get('day_week_' + key)
            if type:
                try:
                    for i in range(1, 10):
                        slot_first = post.get('slot_first_{0}_{1}'.format(str(i), key))
                        slot_second = post.get('slot_second_{0}_{1}'.format(str(i), key))

                        timeSlotsVar = timeSlots()
                        if slot_first and slot_second and timeSlotsVar[slot_first] and timeSlotsVar[slot_second]:
                            first = list(timeSlotsVar).index(f'{slot_first}')
                            second = list(timeSlotsVar).index(f'{slot_second}')
                            if type == 'custom':
                                if first <= second:
                                    timeslotsData.append({
                                        'first': slot_first,
                                        'second': slot_second,
                                        'day': key,
                                        'type': type,
                                    })
                                else:
                                    errors_slot_all.append({
                                        week: ['Time is wrong, please check it again']
                                    })
                except:
                    pass

                if type != 'custom':
                    timeslotsData.append({
                        'first': '',
                        'second': '',
                        'day': key,
                        'type': type,
                    })

        if errors_slot_all:
            return JsonResponse({'success': False, 'errors': errors_slot_all}, status=200, safe=False)

        if not imagesObject:
            images.files.required = True

        if form.is_valid() and images.is_valid() and extra.is_valid() and condition.is_valid():
            post = request.POST
            user_id = request.user.pk

            carForm = form.save(commit=False)
            carForm.brand_id = post.get('brand_id')
            carForm.model_id = post.get('model_id')
            carForm.number_of_seats = post.get('number_of_seats_id')
            carForm.fuel = post.get('fuel_id')
            carForm.transmission = post.get('transmission_id')
            carForm.number_of_doors = post.get('number_of_doors_id')
            carForm.mileage = post.get('mileage_id')
            carForm.user_id = user_id
            carForm.save()

            extrasData = post.getlist('extra_id')
            UserCarsExtra.objects.filter(user_id=user_id, user_car_id=carForm.id).delete()
            for extarId in extrasData:
                UserCarsExtra.objects.create(
                    user_id=user_id,
                    user_car_id=carForm.id,
                    extra_id=extarId,
                )

            from models.functions import handle_uploaded_file
            for file in request.FILES.getlist('files'):
                writenFile = handle_uploaded_file(file)
                if writenFile:
                    Images.objects.create(
                        object_id=carForm.id,
                        type=type_car,
                        path=writenFile,
                    )

            if timeslotsData:
                UserCarsTimeSlot.objects.filter(user_car_id=car_id).all().delete()
                for key, timeslot in enumerate(timeslotsData):
                    UserCarsTimeSlot.objects.create(
                        day=timeslot['day'],
                        type=timeslot['type'],
                        start_hour=timeslot['first'],
                        end_hour=timeslot['second'],
                        user_car_id=carForm.id,
                    )

            rental_duration = post.get('rental_duration')
            min_rental_hours, max_rental_days = rental_duration.split(';')
            conditionSave = UserCarsConditions.objects.filter(user_car_id=carObj.id).first()
            if not conditionSave:
                conditionSave = UserCarsConditions()
                conditionSave.user_car_id = carObj.id
            conditionSave.min_rental_hours = min_rental_hours
            conditionSave.max_rental_days = max_rental_days
            conditionSave.max_booking_notice = max_booking_notice_list().get(post.get('max_booking_notice'))
            conditionSave.preparation_time = post.get('preparation_time')
            conditionSave.special_requirements = post.get('special_requirements')
            conditionSave.save()

        dates = request.POST.get('dates')
        if dates:
            import json
            from car.models import statusAnavailable
            UserCarAvailableDate.objects.filter(
                user_car_id=carObj.id,
            ).all().delete()

            for key, date in json.loads(dates).items():
                for d in date:
                    newObj = json.loads(d)
                    try:
                        if key == statusAnavailable:
                            UserCarAvailableDate.objects.create(
                                time=newObj['time'],
                                status=key,
                                user_car_id=carForm.id,
                                date=newObj['date'],
                            )
                    except:
                        pass

        if request.is_ajax():
            errors_all = []
            if form.errors:
                for field in form:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })

            if condition.errors:
                for field in condition:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })

            if extra.errors:
                for field in extra:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })
            if images.errors:
                for field in images:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })

            if errors_all:
                return JsonResponse({'success': False, 'errors': errors_all}, status=200, safe=False)
            else:
                messages.success(request, _('Car have added successfully'))
                return JsonResponse({'success': True, 'redirect': f'/car/{request.user.id}/list-cars/'}, status=200,
                                    safe=False)
    else:
        form = AddCarForm(instance=carObj)
        extra = AddCarExtrasForm()
        images = UpdateCarImageForm()
        condition = UserCarsConditionsForm()

    brands = CarBrand.objects.all()
    driverLicense = DriverLicense.objects.filter(user_id=request.user.id).first()
    country = dict(countries)[driverLicense.issuing_country] if driverLicense else None

    seats = Feature.objects.filter(status=True, type=type_feature_seat).order_by('-order').all()
    fuels = Feature.objects.filter(status=True, type=type_feature_fuel).order_by('-order').all()
    doors = Feature.objects.filter(status=True, type=type_feature_door).order_by('-order').all()
    transmissions = Feature.objects.filter(status=True, type=type_feature_transmission).order_by('-order').all()
    mileages = Feature.objects.filter(status=True, type=type_feature_mileage).order_by('-order').all()
    extras = Extra.objects.filter(status=True).order_by('-order').all()
    slots = UserCarsTimeSlot.objects.filter(user_car_id=car_id).all()
    slotsList = {}
    if slots:
        for slot in slots:
            if slot.day in slotsList:
                slotsList[slot.day].append({
                    'day': slot.day,
                    'start_hour': slot.start_hour,
                    'end_hour': slot.end_hour,
                    'type': slot.type,
                })
            else:
                slotsList[slot.day] = [{
                    'day': slot.day,
                    'start_hour': slot.start_hour,
                    'end_hour': slot.end_hour,
                    'type': slot.type,
                }]

    imagesData = imagesConfig = ''
    if imagesObject:
        for image in imagesObject:
            imagesData += '"' + str(file_exists(image.path)) + '",'
            imagesConfig += "{'url': '" + reverse('image-delete') + "', 'key':'" + str(
                image.id) + "', 'extra' : {'key':'" + str(image.id) + "','car_id': '" + str(carObj.id) + "'} },"

    max_booking_notice = dict([(value, key) for key, value in max_booking_notice_list().items()])[
        carObj.user_car_conditions().max_booking_notice] if carObj.user_car_conditions() else None

    from car.templatetags.calendar_booking import getDateObjectStr

    return render(request, 'car/step2-edit.html', {
        'imagesData': str(imagesData),
        'imagesConfig': str(imagesConfig),
        'form': form,
        'extra': extra,
        'images': images,
        'max_booking_notice': max_booking_notice,
        'carObj': carObj,
        'dateobj': getDateObjectStr(carObj.id),
        'brands': brands,
        'years': year_first_registration,
        'country': country,
        'slotsList': slotsList,
        'seats': seats,
        'preparationTime': preparationTime,
        'fuels': fuels,
        'mileages': mileages,
        'extras': extras,
        'doors': doors,
        'condition': condition,
        'transmissions': transmissions,
        'weeks': weeks,
        'timeSlots': timeSlots,
    })


@login_required
def car_wizards(request):
    if request.method == 'POST' and request.POST.get('request_from_step2'):
        images = AddCarImageForm(request.POST, request.FILES)
        extra = AddCarExtrasForm(request.POST)
        form = AddCarForm(request.POST)
        condition = UserCarsConditionsForm(request.POST)

        if form.is_valid() and images.is_valid() and extra.is_valid() and condition.is_valid():
            post = request.POST
            errors_slot_all = []
            timeslotsData = []
            for key, week in weeks().items():
                type = post.get('day_week_' + key)
                if type:
                    try:
                        for i in range(1, 10):
                            slot_first = post.get('slot_first_{0}_{1}'.format(str(i), key))
                            slot_second = post.get('slot_second_{0}_{1}'.format(str(i), key))

                            timeSlotsVar = timeSlots()

                            if slot_first and slot_second and timeSlotsVar[slot_first] and timeSlotsVar[slot_second]:

                                first = list(timeSlotsVar).index(f'{slot_first}')
                                second = list(timeSlotsVar).index(f'{slot_second}')
                                if type == 'custom':
                                    if first <= second:
                                        timeslotsData.append({
                                            'first': slot_first,
                                            'second': slot_second,
                                            'day': key,
                                            'type': type,
                                        })
                                    else:
                                        errors_slot_all.append({
                                            week: ['Time is wrong, please check it again']
                                        })
                    except:
                        pass

                    if type != 'custom':
                        timeslotsData.append({
                            'first': '',
                            'second': '',
                            'day': key,
                            'type': type,
                        })

            if errors_slot_all:
                return JsonResponse({'success': False, 'errors': errors_slot_all}, status=200, safe=False)

            user_id = request.user.pk

            carForm = form.save(commit=False)
            carForm.brand_id = post.get('brand_id')
            carForm.model_id = post.get('model_id')
            carForm.number_of_seats = post.get('number_of_seats_id')
            carForm.fuel = post.get('fuel_id')
            carForm.transmission = post.get('transmission_id')
            carForm.displayed_months = post.get('displayed_months') if int(post.get('displayed_months')) % 6 == 0 else 6
            carForm.number_of_doors = post.get('number_of_doors_id')
            carForm.mileage = post.get('mileage_id')
            carForm.user_id = user_id
            carForm.save()

            extrasData = post.getlist('extra_id')
            for extarId in extrasData:
                UserCarsExtra.objects.create(
                    user_id=user_id,
                    user_car_id=carForm.id,
                    extra_id=extarId,
                )

            from models.functions import handle_uploaded_file
            for file in request.FILES.getlist('files'):
                writenFile = handle_uploaded_file(file)
                if writenFile:
                    Images.objects.create(
                        object_id=carForm.id,
                        type=type_car,
                        path=writenFile,
                    )
            if timeslotsData:
                for key, timeslot in enumerate(timeslotsData):
                    UserCarsTimeSlot.objects.create(
                        day=timeslot['day'],
                        type=timeslot['type'],
                        start_hour=timeslot['first'],
                        end_hour=timeslot['second'],
                        user_car_id=carForm.id,
                    )

            rental_duration = post.get('rental_duration')
            min_rental_hours, max_rental_days = rental_duration.split(';')
            UserCarsConditions.objects.create(
                min_rental_hours=min_rental_hours,
                max_rental_days=max_rental_days,
                max_booking_notice=max_booking_notice_list().get(post.get('max_booking_notice')),
                preparation_time=post.get('preparation_time'),
                special_requirements=post.get('special_requirements'),
                user_car_id=carForm.id,
            )

            dates = request.POST.get('dates')
            if dates:
                import json
                from car.models import statusAnavailable
                for key, date in json.loads(dates).items():
                    for d in date:
                        newObj = json.loads(d)
                        try:
                            if key == statusAnavailable:
                                UserCarAvailableDate.objects.create(
                                    time=newObj['time'],
                                    status=key,
                                    user_car_id=carForm.id,
                                    date=newObj['date'],
                                )
                        except:
                            pass

        if request.is_ajax():
            errors_all = []
            if form.errors:
                for field in form:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })
            if condition.errors:
                for field in condition:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })
            if extra.errors:
                for field in extra:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })
            if images.errors:
                for field in images:
                    if field.errors:
                        errors_all.append({
                            field.label: field.errors
                        })

            if errors_all:
                return JsonResponse({'success': False, 'errors': errors_all}, status=200, safe=False)
            else:
                messages.success(request, _('Car have added successfully'))
                return JsonResponse({'success': True, 'redirect': f'/car/{request.user.id}/list-cars/'}, status=200,
                                    safe=False)
    else:
        form = AddCarForm()
        extra = AddCarExtrasForm()
        images = AddCarImageForm()
        condition = UserCarsConditionsForm()

    brands = CarBrand.objects.all()
    post = request.POST
    brand = int(post.get('brand')) if post.get('brand') else None
    model = int(post.get('model')) if post.get('model') else None
    year = int(post.get('year')) if post.get('year') else None
    recommendPrice = math.floor(int(post.get('recommendPrice')) / 20) if post.get('recommendPrice') else None
    millage = int(post.get('millage')) if post.get('millage') else None
    driverLicense = DriverLicense.objects.filter(user_id=request.user.id).first()

    country = dict(countries)[driverLicense.issuing_country] if driverLicense else None
    drivy_open = post.get('drivy_open')

    seats = Feature.objects.filter(status=True, type=type_feature_seat).order_by('-order').all()
    fuels = Feature.objects.filter(status=True, type=type_feature_fuel).order_by('-order').all()
    doors = Feature.objects.filter(status=True, type=type_feature_door).order_by('-order').all()
    transmissions = Feature.objects.filter(status=True, type=type_feature_transmission).order_by('-order').all()
    mileages = Feature.objects.filter(status=True, type=type_feature_mileage).order_by('-order').all()
    extras = Extra.objects.filter(status=True).order_by('-order').all()

    from car.templatetags.calendar_booking import getDateObjectStr

    return render(request, 'car/step2-add.html', {
        'form': form,
        'extra': extra,
        'weeks': weeks,
        'timeSlots': timeSlots,
        'preparationTime': preparationTime,
        'images': images,
        'brands': brands,
        'dateobj': getDateObjectStr(),
        'drivy_open': drivy_open,
        'years': year_first_registration,
        'condition': condition,
        'country': country,
        'brandSelected': brand,
        'modelSelected': model,
        'yearsSelected': year,
        'recommendPrice': recommendPrice,
        'millage': millage,
        'seats': seats,
        'fuels': fuels,
        'mileages': mileages,
        'extras': extras,
        'doors': doors,
        'transmissions': transmissions,
    })


@login_required
def chooseRentType(request):
    if request.method == 'POST':
        calc = Calculation(request.POST)
        if calc.is_valid():
            post = request.POST
            brand = int(post.get('brand')) if post.get('brand') else None
            model = int(post.get('model')) if post.get('model') else None
            year = int(post.get('year')) if post.get('year') else None
            millage = int(post.get('millage')) if post.get('millage') else None

            model = PriceCalculation.objects.filter(
                brand_id=brand,
                model_id=model,
                year=year,
                millage=millage,
            ).first()

            if model:
                priceReplace = model.price.replace('£', '', )
                price = math.floor(int(priceReplace) * int(get_variable('rent_drivy_type')) / 100)
                html = render_to_string('car/rent-type.html', {
                    'csrfmiddlewaretoken': post.get('csrfmiddlewaretoken'), 'price': price, 'model': model,
                    'recommendPrice': priceReplace})

                return JsonResponse({'success': True, 'html': html}, safe=False)

    return JsonResponse({'success': False}, safe=False)


@login_required
def calculatePrice(request):
    if request.method == 'POST':
        calc = Calculation(request.POST)
        if calc.is_valid():
            post = request.POST
            model = PriceCalculation.objects.filter(
                brand_id=int(post.get('brand')),
                model_id=int(post.get('model')),
                year=int(post.get('year')),
                millage=int(post.get('millage')),
            ).first()

            if model:
                return JsonResponse({'success': True, 'allow_redirect': True, 'price': model.price}, safe=False)

    return JsonResponse({'success': False, 'allow_redirect': False}, safe=False)


@login_required
def getModels(request):
    id = request.POST.get('id')

    if request.method == 'POST' and id:
        models = CarModel.objects.filter(brand_id=id).all()
        if models:
            posts_serialized = serializers.serialize('json', models)
            output = [[d['fields'], d['pk']] for d in json.loads(posts_serialized)]
            return JsonResponse({'success': True, 'models': output}, status=200, safe=False)

    return JsonResponse({'success': False}, safe=False)


from django.shortcuts import render
from formtools.wizard.views import SessionWizardView
from django.utils.decorators import method_decorator


class FormWizardView(SessionWizardView):
    template_name = "car/add.html"

    @method_decorator(login_required)
    @method_decorator(checkLoggedInAndRequestUserSame)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def done(self, form_list, **kwargs):
        return render(self.request, 'car/done.html', {
            'form_data': [form.cleaned_data for form in form_list],
        })

    def post(self, request, *args, **kwargs):
        try:
            return self.render(self.get_form())
        except KeyError:
            return super().get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        This method handles GET requests.

        If a GET request reaches this point, the wizard assumes that the user
        just starts at the first step or wants to restart the process.
        The data of the wizard will be resetted before rendering the first step
        """
        self.storage.reset()

        # reset the current step to the first step.
        self.storage.current_step = self.steps.first

        return self.render(self.get_form())
