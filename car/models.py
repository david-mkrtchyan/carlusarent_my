from django.db import models
from django_mysql.models import EnumField
from django.utils import timezone
from users.models import Users as User
from datetime import datetime, date
from models.models import Images, type_car
from django.utils.translation import gettext as _
from models.functions import get_variable


# Create your models here.

class CarBrand(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'car_brands'

    def __str__(self):
        return self.name


class CarModel(models.Model):
    name = models.CharField(max_length=255)
    brand = models.ForeignKey(CarBrand, models.SET_NULL,
                              blank=True,
                              null=True, )

    class Meta:
        db_table = 'car_models'

    def __str__(self):
        return self.name


class PriceCalculation(models.Model):
    brand = models.ForeignKey(CarBrand, on_delete=True)
    model = models.ForeignKey(CarModel, on_delete=True)
    year = models.IntegerField()
    millage = models.IntegerField()
    price = models.CharField(max_length=255)

    class Meta:
        db_table = 'car_price_calculation'


type_feature_seat = 'seat'
type_feature_door = 'door'
type_feature_fuel = 'fuel'
type_feature_transmission = 'transmission'
type_feature_mileage = 'mileage'


class Feature(models.Model):
    name = models.CharField(max_length=100)
    type = EnumField(choices=[type_feature_seat, type_feature_door, type_feature_fuel, type_feature_transmission,
                              type_feature_mileage],
                     default=(type_feature_seat))
    status = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    order = models.PositiveIntegerField(null=True)

    class Meta:
        db_table = 'car_features'


class Extra(models.Model):
    name = models.CharField(max_length=100)
    icon_or_code = models.TextField()
    status = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    order = models.PositiveIntegerField(null=True)

    class Meta:
        db_table = 'car_extras'


with_open = 'with_open'
without_open = 'without_open'


class UserCars(models.Model):
    brand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)
    model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    registration_country = models.CharField(max_length=255, null=True, blank=True)
    year_of_first_register = models.IntegerField()
    number_plate = models.CharField(max_length=255)
    number_of_seats = models.IntegerField()
    number_of_doors = models.IntegerField()
    fuel = models.IntegerField()
    transmission = models.IntegerField()
    description = models.TextField(null=True, blank=True)
    mileage = models.IntegerField()
    admin_confirmed = models.BooleanField(default=False)
    daily_price = models.IntegerField()
    parking_address = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=45)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    with_open = EnumField(choices=[with_open, without_open],
                          default=(without_open))

    two_days_percentage = models.IntegerField(default=int(get_variable('daily_price_discount_2_days_suggestion')))
    one_week_percentage = models.IntegerField(default=int(get_variable('daily_price_discount_1_week_suggestion')))
    one_month_percentage = models.IntegerField(default=int(get_variable('daily_price_discount_1_month_suggestion')))
    displayed_months = models.IntegerField(default=6)
    created_at = models.DateTimeField(default=datetime.now())
    updated_at = models.DateTimeField(default=datetime.now())

    class Meta:
        db_table = 'user_cars'

    def __str__(self):
        return '{0} / {1} {2}'.format(self.user.first_name + ' ' + self.user.last_name, self.brand.name,
                                      self.model.name)

    def image(self):
        return Images.objects.filter(type=type_car, object_id=self.id).order_by('rank').first()

    def images(self):
        return Images.objects.filter(type=type_car, object_id=self.id).order_by('rank').all()

    def seat(self):
        return Feature.objects.filter(id=self.number_of_seats).first()

    def door(self):
        return Feature.objects.filter(id=self.number_of_doors).first()

    def fuelFeature(self):
        return Feature.objects.filter(id=self.fuel).first()

    def transmissionFeature(self):
        return Feature.objects.filter(id=self.transmission).first()

    def mileageFeature(self):
        return Feature.objects.filter(id=self.mileage).first()

    def car_extras(self):
        return UserCarsExtra.objects.filter(user_car_id=self.id, user_id=self.user.id).all()

    def car_extras_lists(self):
        return dict((str(x.extra_id), str(x.extra_id)) for x in
                    UserCarsExtra.objects.filter(user_car_id=self.id, user_id=self.user.id).all())

    def car_favorite(self):
        return UserCarsFavorite.objects.filter(user_id=self.user.id, user_car_id=self.id).first()

    def user_car_conditions(self):
        return UserCarsConditions.objects.filter(user_car_id=self.id).first()


class UserCarsFavorite(models.Model):
    user_car = models.ForeignKey(UserCars, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    favorite = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=datetime.now())
    updated_at = models.DateTimeField(default=datetime.now())

    class Meta:
        db_table = 'user_car_favorites'


statusAvailable = 'available'
statusAnavailable = 'unavailable'


class UserCarAvailableDate(models.Model):
    time = EnumField(choices=['am', 'pm'])
    status = EnumField(choices=[statusAvailable, statusAnavailable])
    user_car = models.ForeignKey(UserCars, on_delete=models.CASCADE)
    date = models.DateField()

    class Meta:
        db_table = 'user_car_available_dates'


class UserCarsTimeSlot(models.Model):
    from .car_functions import weeks, time_slot_type

    day = EnumField(choices=[(i, i) for k, i in enumerate(weeks())])
    type = EnumField(choices=[(i, i) for k, i in enumerate(time_slot_type())], default='custom')
    start_hour = models.CharField(max_length=255, null=True, blank=True)
    end_hour = models.CharField(max_length=255, null=True, blank=True)
    user_car = models.ForeignKey(UserCars, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_car_time_slot'


class UserCarsConditions(models.Model):
    from .car_functions import max_booking_notice_list, preparationTime

    min_rental_hours = models.IntegerField(null=True, blank=True)
    max_rental_days = models.IntegerField(default=30)
    max_booking_notice = EnumField(choices=[(str(i), k) for k, i in max_booking_notice_list().items()], null=True,
                                   blank=True)
    preparation_time = EnumField(choices=[(i, i) for k, i in enumerate(preparationTime())], null=True, blank=True)

    special_requirements = models.CharField(null=True, blank=True, max_length=255)

    user_car = models.ForeignKey(UserCars, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_car_conditions'


class UserCarsExtra(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_car = models.ForeignKey(UserCars, on_delete=models.CASCADE)
    extra = models.ForeignKey(Extra, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_car_extras'
