# Generated by Django 2.2.2 on 2019-07-05 20:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('car', '0002_auto_20190706_0003'),
    ]

    operations = [
        migrations.RenameField(
            model_name='carmodel',
            old_name='makeyear',
            new_name='brand',
        ),
    ]
