# Generated by Django 2.2.2 on 2019-07-21 19:36

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car', '0026_auto_20190721_2336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercars',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 232044)),
        ),
        migrations.AlterField(
            model_name='usercars',
            name='end_date',
            field=models.DateField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 232044)),
        ),
        migrations.AlterField(
            model_name='usercars',
            name='start_date',
            field=models.DateField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 232044)),
        ),
        migrations.AlterField(
            model_name='usercars',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 232044)),
        ),
        migrations.AlterField(
            model_name='usercarsfavorite',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 234038)),
        ),
        migrations.AlterField(
            model_name='usercarsfavorite',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 21, 23, 36, 47, 234038)),
        ),
        migrations.AlterModelTable(
            name='usercarsfavorite',
            table='user_car_favorites',
        ),
    ]
