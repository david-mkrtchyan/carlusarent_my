# Generated by Django 2.2.2 on 2019-07-13 21:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car', '0017_auto_20190714_0028'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercars',
            name='admin_confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='usercars',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 14, 1, 41, 30, 869815)),
        ),
        migrations.AlterField(
            model_name='usercars',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 7, 14, 1, 41, 30, 869815)),
        ),
    ]
