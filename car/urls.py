from django.urls import path
from . import views

urlpatterns = [
    path('<int:id>/add-car', views.addCar, name='add-car'),
    path('get-models/', views.getModels, name='get-models'),
    path('calculate-price/', views.calculatePrice, name='calculate-price'),
    path('choose-rent-type/', views.chooseRentType, name='choose-rent-type'),
    path('car_wizards/new/', views.car_wizards, name='car-wizards-new'),
    path('car_wizards/<int:id>/edit/<int:car_id>', views.car_wizards_edit, name='car-wizards-edit'),
    path('car_wizards/<int:id>/delete/', views.deleteCar, name='car-wizards-delete'),
    path('car_wizards/<int:id>/view', views.viewCar, name='car-wizards-view'),
    path('<int:id>/list-cars/', views.cars, name='list-cars'),
    path('list-favorites/', views.listFavorite, name='list-favorites'),
    path('add-favorite/', views.addFavorite, name='add-favorite'),
    path('<int:id>/car-view/', views.carview, name='car-view'),
    path('add-calendar/', views.moreCalendar, name='add-calendar'),
    path('add-calendar-date/', views.addCalendarDate, name='add-calendar-date'),
    path('edit-calendar-date/', views.editCalendarDate, name='edit-calendar-date'),
    # path('<int:id>/person-cars/', views.personCar, name='person-cars'),
]
