from django.contrib import admin
from .models import CarBrand, CarModel, Feature, Extra, UserCars
from .models import type_feature_seat
from django.contrib.admin.filters import AllValuesFieldListFilter
from django.utils.translation import gettext as _

# Register your models here.

class DropdownFilter(AllValuesFieldListFilter):
    template = 'admin/car/usercars/dropdown_filter.html'
    model = Feature.objects.all()

    def choices(self, changelist):
        yield {
            'selected': self.lookup_val is None and self.lookup_val_isnull is None,
            'query_string': changelist.get_query_string(remove=[self.lookup_kwarg, self.lookup_kwarg_isnull]),
            'display': _('All'),
        }
        include_none = False
        for val in self.lookup_choices:
            if val is None:
                include_none = True
                continue
            val = str(val)
            yield {
                'selected': self.lookup_val == val,
                'query_string': changelist.get_query_string({self.lookup_kwarg: val}, [self.lookup_kwarg_isnull]),
                'display': val,
            }
        if include_none:
            yield {
                'selected': bool(self.lookup_val_isnull),
                'query_string': changelist.get_query_string({self.lookup_kwarg_isnull: 'True'}, [self.lookup_kwarg]),
                'display': self.empty_value_display,
            }


@admin.register(UserCars)
class UserCarsAdmin(admin.ModelAdmin):
    list_filter = (
        ('number_of_seats', DropdownFilter),
        'number_plate', 'fuel', 'transmission', 'year_of_first_register',
        'admin_confirmed', 'daily_price', 'user')

    list_display = (
        'user', 'daily_price', 'number_plate', 'number_of_seats', 'fuel', 'transmission', 'year_of_first_register',
        'admin_confirmed',)


@admin.register(CarBrand)
class DistrictAdmin(admin.ModelAdmin):
    sortable = 'name'
    list_filter = ('name',)


@admin.register(CarModel)
class DistrictAdmin(admin.ModelAdmin):
    sortable = 'name'
    list_filter = ('name', 'brand')
    fields = ['name', 'brand']
    list_display = ('name', 'brand',)


@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    sortable = 'order'
    list_filter = ('order', 'name', 'type', 'status')

    fields = ['name', 'type', 'status']
    # readonly_fields = ['image_tag', ]
    list_display = ('name', 'type', 'status',)


@admin.register(Extra)
class ExtraAdmin(admin.ModelAdmin):
    sortable = 'order'
    list_filter = ('order', 'name', 'status')

    fields = ['name', 'icon_or_code', 'status']
    # readonly_fields = ['image_tag', ]
    list_display = ('name', 'status',)
