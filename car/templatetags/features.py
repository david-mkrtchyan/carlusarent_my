from django import template
from django.core.files.storage import default_storage
from django.core.files import File
import sys
from carlusarent import settings
from pathlib import Path
from PIL import Image
from car.models import Feature

register = template.Library()


@register.simple_tag()
def get_feature(id):
    f = Feature.objects.filter(id=id).first()
    if f:
        return f.name


register.filter(get_feature)
