from django import template
import calendar
import time
from datetime import datetime, date, timedelta
from dateutil import relativedelta
import re
from car.models import UserCarAvailableDate

register = template.Library()


def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m - 1, 12)
    return date(y + a, m + 1, 1)


def get_last_day(dt):
    return get_first_day(dt, 0, 1) + timedelta(-1)


def getDates():
    return {
        "Mon": 1,
        "Tue": 2,
        "Wed": 3,
        "Thu": 4,
        "Fri": 5,
        "Sat": 6,
        "Sun": 0,
    }


@register.inclusion_tag('templatetags/calendar.html')
def calendar_data(addDate=None, dateobj=None, displayed_months=6):
    if addDate:
        datestart = addDate
    else:
        datestart = date.today()

    # start
    days = getDates()

    year_first, month_first, day_first, week_day, month_long = get_first_day(datestart).strftime(
        "%Y %m %d %a %B").split()
    year_last, month_last, day_last = get_last_day(datestart).strftime("%Y %m %d").split()
    year_to, month_to, day_to = map(int, time.strftime("%Y %m %d").split())
    firstcalendar = {}
    leftFirst = days[week_day] - 1

    for i in range(int(day_first), int(day_last) + 1):
        key2 = month_long + ' ' + str(year_first)
        if key2 not in firstcalendar:
            firstcalendar[key2] = {}

        year2, month2, day2, week2 = datetime(int(year_first), int(month_first), int(i)).strftime(
            "%Y %m %d %a").split()

        keyday = day2.replace('0', '') if day2[:1] == '0' else day2

        if addDate or int(day2) >= int(day_to):
            firstcalendar[key2].update({
                keyday: [
                    {"date": f"{year2}-{month2}-{day2}", "time": "am", "weekDay": days[week2]},
                    {"date": f"{year2}-{month2}-{day2}", "time": "pm", "weekDay": days[week2]},
                ]
            })
        else:
            firstcalendar[key2].update({
                keyday: [
                    {"date": f"{year2}-{month2}-{day2}", "time": "am", "weekDay": days[week2], 'disabled': True},
                    {"date": f"{year2}-{month2}-{day2}", "time": "pm", "weekDay": days[week2], 'disabled': True},
                ]
            })

    lists = {}

    for j in range(1, displayed_months):
        nextmonth = datestart + relativedelta.relativedelta(months=j)
        year2, month2, day2, hour2, minute2 = nextmonth.strftime("%Y %m %d %H %M ").split()
        year_first2, month_first2, day_first2, week_day2, month_long = get_first_day(nextmonth).strftime(
            "%Y %m %d %a %B").split()
        year_last2, month_last2, day_last2 = get_last_day(nextmonth).strftime("%Y %m %d").split()
        secondcalendar = {}
        leftsecond = days[week_day2] - 1

        for i in range(int(day_first2), int(day_last2) + 1):
            key2 = month_long + ' ' + str(year_first2)
            if key2 not in secondcalendar:
                secondcalendar[key2] = {}

            year2, month2, day2, week2 = datetime(int(year_first2), int(month_first2), int(i)).strftime(
                "%Y %m %d %a").split()

            keyday = day2.replace('0', '') if day2[:1] == '0' else day2

            secondcalendar[key2].update({
                keyday: [
                    {"date": f"{year2}-{month2}-{day2}", "time": "am", "weekDay": days[week2]},
                    {"date": f"{year2}-{month2}-{day2}", "time": "pm", "weekDay": days[week2]},
                ]
            })

        data = {
            'secondcalendar': secondcalendar,
            'leftsecond': range(leftsecond),
        }

        if j == 5:
            nextmonth = datetime(int(year2), int(month2), int(day2)) + relativedelta.relativedelta(months=1)
            year7, month7, day7 = nextmonth.strftime("%Y %m %d").split()
            next_date = year7 + '-' + month7 + '-' + day7

        lists[j] = data
    # end date

    return {
        'firstcalendar': firstcalendar,
        'secondcalendar': secondcalendar,
        'displayed_months': displayed_months,
        'leftFirst': range(leftFirst),
        'lists': lists,
        'dateobj': dateobj,
        'year_month_day': next_date
    }


def getDateObjectStr(car_id=None):
    from datetime import date
    from car.models import statusAnavailable
    lastmounth = datetime.today() + relativedelta.relativedelta(months=5)
    lastmounth = get_last_day(lastmounth).strftime('%Y-%m-%d')
    today = get_first_day(date.today()).strftime('%Y-%m-%d')
    start = datetime.strptime(today, "%Y-%m-%d")
    end = datetime.strptime(lastmounth, "%Y-%m-%d")
    date_array = (start + timedelta(days=x) for x in range(0, (end + timedelta(days=1) - start).days))

    dateobj = '{'
    for date_object in date_array:
        date = date_object.strftime("%Y-%m-%d")
        am = '{&quot;time&quot;:&quot;am&quot;,&quot;availability&quot;:&quot;available&quot;}'
        pm = '{&quot;time&quot;:&quot;pm&quot;,&quot;availability&quot;:&quot;available&quot;}'
        if car_id:
            modeltimeam = UserCarAvailableDate.objects.filter(user_car_id=car_id,
                                                              date=date,
                                                              status=statusAnavailable,
                                                              time='am').first()

            if modeltimeam:
                am = '{&quot;time&quot;:&quot;am&quot;,&quot;availability&quot;:&quot;unavailable&quot;}'

            modeltimepm = UserCarAvailableDate.objects.filter(user_car_id=car_id,
                                                              date=date,
                                                              status=statusAnavailable,
                                                              time='pm').first()
            if modeltimepm:
                pm = '{&quot;time&quot;:&quot;pm&quot;,&quot;availability&quot;:&quot;unavailable&quot;}'

        dateobj += '&quot;' + str(date) + '&quot;:[' + am + ',' + pm + '],'
    dateobj += '}'
    dateobj = dateobj.rsplit(',', 1)
    dateobj2 = dateobj[0] + dateobj[1]

    return dateobj2
