from django import template
from django.core.files.storage import default_storage
from django.core.files import File
import sys
from carlusarent import settings
from pathlib import Path
from PIL import Image

register = template.Library()


@register.simple_tag()
def get_val(slotsList, key):
    return slotsList.get(key)


register.filter(get_val)
