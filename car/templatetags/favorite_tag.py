from django import template
from car.models import UserCarsFavorite

register = template.Library()


@register.simple_tag()
def get_favorite(userId, carId, favorite=True):
    return UserCarsFavorite.objects.filter(user_id=userId, user_car_id=carId,
                                           favorite=favorite).first()

register.filter(get_favorite)
