from django import template
from django.core.files.storage import default_storage
from django.core.files import File
import sys
from carlusarent import settings
from pathlib import Path
from PIL import Image

register = template.Library()


@register.filter(name='file_exists')
def file_exists(filename,type='car'):
    filepathBig = f'/media/uploads/big/{type}/{filename}'
    filepathSmall = f'/media/uploads/small/{type}/{filename}'
    try:
        path = bytes(sys.path[0] + str(filepathBig), "utf-8")
        image = Image.open(path)
        filepath = filepathBig
    except FileNotFoundError as e:
        try:
            path = bytes(sys.path[0] + str(filepathSmall), "utf-8")
            image = Image.open(path)
            filepath = filepathSmall
        except FileNotFoundError as e:
            return settings.MEDIA_URL + 'uploads/default_car.png'
    except Exception as e:
        return False
    else:
        return filepath


register.filter(file_exists)
