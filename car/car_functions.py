import time
from .models import with_open, without_open
from django.utils.translation import gettext as _


def millages():
    mileages = {
        1: '0-10,000 mi',
        2: '10-30,000 mi',
        3: '30-60,000 mi',
        4: '60-90,000 mi',
        5: '90-130,000 mi',
        6: '130,000 mi+',
    }

    return mileages


def open():
    return {
        with_open: with_open,
        without_open: without_open,
    }


def years():
    years = []
    for i in range(2010, int(time.strftime("%Y")) + 1):
        years.append(i)

    return years


def year_first_registration():
    years = []
    for i in range(1930, int(time.strftime("%Y")) + 1):
        years.append(i)

    return reversed(years)


def weeks():
    return {
        'monday': _('Monday'),
        'tuesday': _('Tuesday'),
        'wednesday': _('Wednesday'),
        'thursday': _('Thursday'),
        'friday': _('Friday'),
        'saturday': _('Saturday'),
        'sunday': _('Sunday'),
    }


def time_slot_type():
    return {
        'never': _('Never'),
        'always': _('24 hours a day'),
        'custom': _('At fixed times'),
    }


def max_booking_notice_list():
    return {
        'The next 15 days': '15',
        'The next month': '+1 month',
        'The next 3 months': '3',
        'The next 6 months': '6',
        'All future dates': ''
    }


def timeSlots():
    return {
        '00:00': '00:00',
        '05:00': '05:00',
        '05:30': '05:30',
        '06:00': '06:00',
        '06:30': '06:30',
        '07:00': '07:00',
        '07:30': '07:30',
        '08:00': '08:00',
        '08:30': '08:30',
        '09:00': '09:00',
        '09:30': '09:30',
        '10:00': '10:00',
        '10:30': '10:30',
        '11:00': '11:00',
        '11:30': '11:30',
        '12:00': '12:00',
        '12:30': '12:30',
        '13:00': '13:00',
        '13:30': '13:30',
        '14:00': '14:00',
        '14:30': '14:30',
        '15:00': '15:00',
        '15:30': '15:30',
        '16:00': '16:00',
        '16:30': '16:30',
        '17:00': '17:00',
        '17:30': '17:30',
        '18:00': '18:00',
        '18:30': '18:30',
        '19:00': '19:00',
        '19:30': '19:30',
        '20:00': '20:00',
        '20:30': '20:30',
        '21:00': '21:00',
        '21:30': '21:30',
        '22:00': '22:00',
        '22:30': '22:30',
        '23:00': '23:00',
        '23:30': '23:30',
    }


def preparationTime():
    return {
        '': _('Last-minute requests accepted'),
        '30': _('30 minutes minimum'),
        '60': _('1 hour minimum'),
        '120': _('2 hours minimum'),
        '180': _('3 hours minimum'),
        '360': _('6 hours minimum'),
        '720': _('12 hours minimum'),
        '1440': _('1 day minimum'),
        '2880': _('2 days minimum'),
        '4320': _('3 days minimum'),
        '10080': _('7 days minimum'),
    }
