from functools import wraps


def checkLoggedInAndRequestUserSame(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if int(request.user.id) != int(kwargs.get('id')):
            from django.http import HttpResponseForbidden
            return HttpResponseForbidden()
        else:
            response = view_func(request, *args, **kwargs)
            return response

    return _wrapped_view


def isSuperUser(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if int(request.user.is_superuser):
            response = view_func(request, *args, **kwargs)
            return response
        else:
            from django.http import HttpResponseForbidden
            return HttpResponseForbidden()

    return _wrapped_view
