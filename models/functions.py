from .models import Variable


def get_variable(key, object=False):
    varibale = Variable.objects.filter(key=key).first()
    if varibale:
        if object == True:
            return varibale
        return varibale.value


def handle_uploaded_file(f):
    import os
    import secrets
    from carlusarent.settings import MEDIA_ROOT, STATIC_ROOT,BASE_DIR

    _, f_ext = os.path.splitext(f.name)
    originFile = None
    try:
        dirname = BASE_DIR+'/media/uploads/'

        os.makedirs(dirname, mode=0o777, exist_ok=True)

        random_hex = secrets.token_hex(8)
        file_name = random_hex + f_ext
        originFile = f'{dirname}{file_name}'
        with open(originFile, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
    except:
        pass

    return originFile
