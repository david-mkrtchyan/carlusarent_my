from django.contrib import admin
from .models import Variable


# Register your models here.

@admin.register(Variable)
class VariableAdmin(admin.ModelAdmin):
    sortable = 'order'
    list_filter = ('order', 'name', 'value', 'updated_at', 'type')

    fields = ['order', 'name', 'value', 'updated_at', 'type']
    # readonly_fields = ['image_tag', ]
    list_display = ('order', 'name', 'value', 'updated_at', 'type',)

