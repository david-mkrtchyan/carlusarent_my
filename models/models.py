from django.db import models
from django_mysql.models import EnumField
from django.utils import timezone
from PIL import Image
import secrets
import os
from django.utils.translation import gettext as _
from carlusarent.settings import  BASE_DIR

type_profile = 'profile'
type_car = 'car'
type_about = 'about'

type_variable_number = 'number'
type_variable_text = 'text'
type_variable_image = 'image'


class Variable(models.Model):
    name = models.CharField(max_length=100)
    key = models.SlugField()
    value = models.TextField()
    title = models.TextField(null=True, blank=True)
    type = EnumField(choices=[type_variable_number, type_variable_text, type_variable_image],
                     default=(type_variable_text))
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)
    order = models.PositiveIntegerField(null=True)

    class Meta:
        db_table = 'variables'


class Images(models.Model):
    object_id = models.IntegerField()
    type = EnumField(choices=[type_profile, type_car, type_about], default=(type_car))
    path = models.ImageField(upload_to='uploads/')
    rank = models.IntegerField(default=0, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(timezone.now, default=timezone.now)

    class Meta:
        db_table = 'images'

    def save(self, *args, **kwargs):
        if self.type != 'car':
            super().save(*args, **kwargs)

        if self.path:

            img = Image.open(self.path.path)

            random_hex = secrets.token_hex(8)
            _, f_ext = os.path.splitext(img.filename)
            picture_fn = random_hex + f_ext

            bigsize = f'{BASE_DIR}/media/uploads/big/{self.type}/'
            picture_path = os.path.join(bigsize, picture_fn)
            img.save(picture_path)
            if os.path.isfile(self.path.path):
                os.remove(self.path.path)
                self.path = f'{picture_fn}'
                super().save(*args, **kwargs)

            if img.width > 200 or img.height > 200:
                picture_path = os.path.join(f'{BASE_DIR}/media/uploads/small/{self.type}/', picture_fn)
                output_size = (200, 200)
                img.thumbnail(output_size)
                img.save(picture_path)
