from django_seed import Seed
from .models import Variable, type_variable_number, type_variable_text


class RunSeed(Seed):

    def __init__(self):
        super().__init__()

    def __run__(self):
        rent_drivy_type = Variable.objects.filter(key='rent_drivy_type').first()
        if not rent_drivy_type:
            self.add(Variable(), {
                'name': 'Percentage for "How do you want to rent" ',
                'key': 'rent_drivy_type',
                'value': '40',
                'type': type_variable_number,
            })

        drive_open_estimation_earning = Variable.objects.filter(key='drive_open_estimation_earning').first()
        if not drive_open_estimation_earning:
            self.add(Variable(), {
                'name': 'Drivy Open (Estimation of your earnings) ',
                'title': 'ESTIMATED MONTHLY EARNINGS',
                'key': 'drive_open_estimation_earning',
                'value': 'This is an average estimate based on the price recommended for your car, renting it 20 days per month. The estimate assumes that your car can be Drivy Open installed, and that your car\'s insurance category is accepted on the Drivy platform. You can check that here.',
                'type': type_variable_text,
            })

        drive_estimation_earning = Variable.objects.filter(key='drive_estimation_earning').first()
        if not drive_estimation_earning:
            self.add(Variable(), {
                'name': 'Drivy (Estimation of your earnings) ',
                'title': 'ESTIMATED MONTHLY EARNINGS',
                'key': 'drive_estimation_earning',
                'value': 'Average estimate based on the price recommended for your car by Drivy, renting it 10 days per month. What you actually earn varies depending on a range of factors, such as your location and the time of the year.',
                'type': type_variable_text,
            })

        daily_price_discount_2_days_minimum = Variable.objects.filter(key='daily_price_discount_2_days_minimum').first()
        if not daily_price_discount_2_days_minimum:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 2 days minimum percent',
                'title': 'Daily Price Discount for 2 days minimum percent',
                'key': 'daily_price_discount_2_days_minimum',
                'value': '10',
                'type': type_variable_text,
            })

        daily_price_discount_2_days_suggestion = Variable.objects.filter(key='daily_price_discount_2_days_suggestion').first()
        if not daily_price_discount_2_days_suggestion:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 2 days suggestion percent',
                'title': 'Daily Price Discount for 2 days suggestion percent',
                'key': 'daily_price_discount_2_days_suggestion',
                'value': '15',
                'type': type_variable_text,
            })

        daily_price_discount_1_week_minimum = Variable.objects.filter(key='daily_price_discount_1_week_minimum').first()
        if not daily_price_discount_1_week_minimum:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 1 week minimum percent',
                'title': 'Daily Price Discount for 1 week minimum percent',
                'key': 'daily_price_discount_1_week_minimum',
                'value': '25',
                'type': type_variable_text,
            })

        daily_price_discount_1_week_suggestion = Variable.objects.filter(key='daily_price_discount_1_week_suggestion').first()
        if not daily_price_discount_1_week_suggestion:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 1 week suggestion percent',
                'title': 'Daily Price Discount for 1 week suggestion percent',
                'key': 'daily_price_discount_1_week_suggestion',
                'value': '40',
                'type': type_variable_text,
            })

        daily_price_discount_1_month_minimum = Variable.objects.filter(key='daily_price_discount_1_month_minimum').first()
        if not daily_price_discount_1_month_minimum:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 1 month minimum percent',
                'title': 'Daily Price Discount for 1 month minimum percent',
                'key': 'daily_price_discount_1_month_minimum',
                'value': '35',
                'type': type_variable_text,
            })

        daily_price_discount_1_month_suggestion = Variable.objects.filter(key='daily_price_discount_1_month_suggestion').first()
        if not daily_price_discount_1_month_suggestion:
            self.add(Variable(), {
                'name': 'Daily Price Discount for 1 month suggestion percent',
                'title': 'Daily Price Discount for 1 month suggestion percent',
                'key': 'daily_price_discount_1_month_suggestion',
                'value': '57',
                'type': type_variable_text,
            })

        preparation_time = Variable.objects.filter(key='preparation_time').first()
        if not preparation_time:
            self.add(Variable(), {
                'name': 'Preparation time',
                'title': 'Preparation time',
                'key': 'preparation_time',
                'value': 'We recommend accepting last-minute requests, but you are free to specify that you need advance notice.',
                'type': type_variable_text,
            })

        maximum_booking_notice = Variable.objects.filter(key='maximum_booking_notice').first()
        if not preparation_time:
            self.add(Variable(), {
                'name': 'Maximum booking notice',
                'title': 'Maximum booking notice',
                'key': 'maximum_booking_notice',
                'value': 'We recommend leaving all the future dates available, but you are free to decline any requests made too far in advance.',
                'type': type_variable_text,
            })

        special_requirements = Variable.objects.filter(key='special_requirements').first()
        if not special_requirements:
            self.add(Variable(), {
                'name': 'Special requirements',
                'title': 'Special requirements',
                'key': 'special_requirements',
                'value': 'Are you easy-going or obsessive about cleanliness? Do you accept pets?',
                'type': type_variable_text,
            })

    def add(self, model, obj):
        for key, ob in obj.items():
            setattr(model, key, ob)
        model.save()
