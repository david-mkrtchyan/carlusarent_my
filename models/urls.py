from django.urls import path
from . import views

urlpatterns = [
    path('run-seed/', views.runSeed, name='run-seed'),
]
