from django.shortcuts import render
from .seed import RunSeed
from decorators.own_decorator import isSuperUser
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required
@isSuperUser
def runSeed(request):
    RunSeed().__run__()
    return render(request, 'seed/completed-seed.html')
