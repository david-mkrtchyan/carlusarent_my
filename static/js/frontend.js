$(document).ready(function () {

    if ($('#language-list').length) {
        new Vue({
            el: '#language-list',
            methods: {
                set_lang: function (e) {
                    e.preventDefault()
                    let name = e.target.value;
                    let url = '/i18n/setlang/'
                    let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();

                    const options = {
                        method: 'post',
                        params: {
                            language: name,
                            csrfmiddlewaretoken: csrftoken,
                        },
                        responseType: 'json',
                    };

                    $.post(url, {language: name, csrfmiddlewaretoken: csrftoken}, function (data) {
                        location.reload();
                    });

                }
            }
        });
    }


    $(document).on('change', '.brand-model', function (e) {
        e.preventDefault()
        let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();

        $.post('/car/get-models/', {id: $(this).val(), csrfmiddlewaretoken: csrftoken}, function (data) {
            $('.model-import').find('option').remove()
            if (data.success == true) {
                $.each(data.models, function (k, v) {
                    $('.model-import').append('<option value="' + v[1] + '">' + v[0].name + '</option>')
                });

                var modelSelected = $('.model-import').attr('data-selected')
                if (modelSelected) {
                    $(".model-import option").each(function () {
                        if ($(this).val() == modelSelected) {
                            $(this).attr("selected", "selected");
                        }
                    });
                }

                changeTitleBrandModel()

            } else {
                $('.model-import').append('<option>' + gettext('Model') + '</option>')
            }

            setTimeout(function () {
                jQuery('.model-import').change()
            }, 4)

        });
    });

    $('.favourite').click(function () {
        var thisd = $(this)

        if (!$(this).data("login")) {
            window.open('/auth/login/', '_blank');
            return
        }

        let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();
        $.post('/car/add-favorite/', {id: $(this).data("id"), csrfmiddlewaretoken: csrftoken}, function (data) {
            if (data && data.success) {
                if (data.favorite == true) {
                    thisd.find('i').removeClass('fa-heart-o')
                    thisd.find('i').addClass('fa-heart')

                    if (thisd.closest('.favourite-view').length) {
                        thisd.find('.add-to-favorite').text(gettext('Remove from favorites'))
                    }

                } else {
                    if (thisd.closest('.favorites-lists').length) {
                        thisd.closest('.favorite-item').remove()
                    }

                    if (thisd.closest('.favourite-view').length) {
                        thisd.find('.add-to-favorite').text(gettext('Add to favorites'))
                    }
                    thisd.find('i').addClass('fa-heart-o')
                    thisd.find('i').removeClass('fa-heart')

                }
            }

        });
    })

    $('.model-import').change(function () {
        changeTitleBrandModel()
    })

    $('.flex_row_center').find('.cobalt-Stepper__ActionButton').click(function (e) {
        e.preventDefault()
        var th = $(this)
        var type = th.attr('data-type')
        var suggestButton = th.closest('.flex_row_center').find('.cobalt-Button--ghost')
        var percentDiv = th.closest('.cobalt-Stepper__Wrapper').find('.price-percent')
        var percentBlog = th.closest('.cobalt-Stepper__Wrapper').find('.cobalt-flexAlign')
        var percent_input = th.closest('.cobalt-Stepper__Wrapper').find('.percent_input')
        var min_percent_input = percent_input.attr('data-min')
        var prcie = parseInt(percentDiv.text())
        th.closest('.cobalt-Stepper__Wrapper').find('.cobalt-Stepper__ActionButton').removeClass('cobalt-Button--disabled')
        percentBlog.removeClass('danger')

        if (type == 'plus') {
            prcie += 1
        } else {
            prcie -= 1
        }

        if (min_percent_input > prcie) {
            th.addClass('cobalt-Button--disabled')
            percentBlog.addClass('danger')
            return false
        }

        var suggestprice = parseInt(suggestButton.find('.suggest-price').text())
        if (suggestprice == prcie) {
            suggestButton.addClass('hide')
        } else {
            suggestButton.removeClass('hide')
        }

        percent_input.val(prcie)

        percentDiv.text(prcie)
    })
    $('.flex_row_center').find('.cobalt-Stepper__ActionButton').trigger('click')
    $('.cobalt-Button--standalone-price').click(function (e) {
        e.preventDefault()
        var th = $(this)
        var suggprice = parseInt(th.find('.suggest-price').text())
        th.closest('.flex_row_center').find('.price-percent').text(suggprice)
        var percent_input = th.closest('.flex_row_center').find('.percent_input')
        percent_input.val(suggprice)
        th.addClass('hide')
    })

    function changeTitleBrandModel() {
        var titleModal = $('#title-modal-brand')
        if (titleModal.length) {
            var slBrand = $(".brand-model option:selected").text();
            var slModal = $(".model-import option:selected").text();
            titleModal.find('.js_title_preview').text(slBrand + ' ' + slModal)
            titleModal.removeClass('hide')
        }
    }

    jQuery('.brand-model').change()

    jQuery('#calculation-form').find('select,input').change(function (e) {
        let dataForm = jQuery('#calculation-form').serialize()
        $('#calculation-form').removeClass('not_redirect')
        $.post('/car/calculate-price/', dataForm, function (data) {
            if (data.success == true) {
                $('.price-calculated').html(data.price)
                $('#calculation-form').addClass('not_redirect')
            } else {
                $('.price-calculated').html('')
            }
        })
    });

    jQuery('.calc-button ').click(function (e) {
        let dataForm = jQuery('#calculation-form').serialize()
        $.post('/car/choose-rent-type/', dataForm, function (data) {
            if (data.success == true) {
                $('.calculation-dest').html(data.html)
            } else {
                $('.calculation-dest').html('')
            }

            $(document).on('click', '.cobalt-Button--fullWidth', function (e) {
                $('.drivy_open').val($(this).attr('data-set'))
            });
        })

    });


    $(document).on('click', '.cobalt-Icon--colorGraphiteLight', function () {
        let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();
        $.post('/variable-modal/', {
            'key': $(this).attr('data-val'),
            'class': 'monthly_earnings',
            csrfmiddlewaretoken: csrftoken
        }, function (data) {
            if (data.success == true && data.html) {
                $('.blog-modal').html(data.html)
                $("#exampleModal").modal('show')
            }
        })
    })

    $('.add-time-slot').click(function (e) {
        e.preventDefault()
        // var sec = $(this).closest('.group-section-weeks').find('.second-group-time_slot')
        // if (sec.hasClass('hide')) {
        //     sec.removeClass('hide')
        //     $(this).text(gettext('Delete time slot'))
        // } else {
        //     sec.show("slide", {direction: "right"}, 1000);
        //     sec.addClass('hide')
        //     $(this).text(gettext('Add a time slot'))
        // }
    });
    $('.checkin-checkout-title').click(function (e) {
        e.preventDefault()
        var sec = $('.checkin-checkout')
        if (sec.hasClass('hide')) {
            sec.removeClass('hide')
        } else {
            sec.addClass('hide')
        }
    });


    $(".add-more").click(function () {
        var closesDiv = $(this).closest('.group-section-weeks')
        var slots = closesDiv.find(".copy").find('.slot-clone');
        var copy = closesDiv.find(".copy")
        slots.each(function () {
            var attr = $(this).attr('name')
            var words = attr.split("_")
            var newName = words[0] + '_' + words[1] + '_' + (parseInt(words[2]) + 1) + '_' + words[3]
            $(this).attr('name', newName)
        })

        var html = copy.html();
        var sec = closesDiv.find('.second-group-time_slot')
        sec.after(html);

    });


    $('.select-type-slot').each(function (e) {
        $(this).change(function (e) {
            var slot = $(this).closest('.group-section-weeks').find('.slot-time')
            var addtimeslot = $(this).closest('.group-section-weeks').find('.add-time-slot')
            var controlgroup = $(this).closest('.group-section-weeks').find('.control-group')
            if ($(this).val() != 'custom') {
                slot.addClass('hide')
                addtimeslot.addClass('hide')
                controlgroup.addClass('hide')
            } else {
                slot.removeClass('hide')
                controlgroup.removeClass('hide')
                addtimeslot.removeClass('hide')
            }
        })
    })
    $('.select-type-slot').change()


    $("body").on("click", ".remove-slot", function () {
        $(this).closest(".control-group").remove();
    });

    $(document).on('click', '.content-calendar td', function (e) {
        e.preventDefault()
        var thisa = $(this)
        if (thisa.hasClass('noday') || thisa.hasClass('disabled-calendar')) {
            return false
        }
        var val = thisa.text()
        var month = thisa.closest('table').find('.month').text()
        var active = thisa.closest('.content-calendar').find('td.start-td')
        if (!active.length) {
            thisa.addClass('start-td')
        } else {
            thisa.addClass('end-td')
        }
    })

    $(document).on('click', '.js_show_more_months', function () {
        let val = $('.year_month_day').attr('data-last')
        let displayed_months2 = $('.displayed_months').val()
        let csrftoken = $('input[name="csrfmiddlewaretoken"]').val();
        var l = parseInt($('.js_car_calendar').attr('data-months'))
        $.ajax({
            method: "post",
            url: '/car/add-calendar/',
            dataType: "json",
            data: {year_month_day: val, csrfmiddlewaretoken: csrftoken,'displayed_months':displayed_months2},
            success: function (data) {
                l += 6
                $('.js_car_calendar').attr("data-months", l)

                if (data.success == true) {
                    $('.owner_calendar_wrapper').append(data.html)
                    $('.year_month_day').attr('data-last', data.nextmonth)
                    $('.displayed_months').remove()
                    calendar()
                }
            }
        }), !1
    })

    function calendar() {
        if ($('.js_car_calendar ').length) {
            var i = 0
            $('.owner_calendar_period').each(function () {
                if (!$(this).hasClass('disabled')) {
                    $(this).attr('data-index', i)
                    i += 1
                }
            })
        }
    }


    if ($('.owner_calendar_period').length) {
        var d = new Date();
        var day = d.getDate();
        if ($('.owner_calendar_period:not(.disabled)')) {
            var disableddate = day * 2
            for (var i = 2; i < disableddate; i++) {
                var disBlock = $('.owner_calendar_period:not(.disabled)')
                if (disBlock.eq(0).length) {
                    disBlock.eq(0).addClass('disabled')
                    disBlock.eq(0).removeClass('selectable_period unavailable available')
                }
            }
        }
        $('.owner_calendar_wrapper').css({'display':'flex'})

        if($('.displayed_months').length)
        {
            console.log($('.displayed_months').val())
            if($('.displayed_months').val() > 6){
                  $('.js_show_more_months').trigger('click')
            }
        }
    }

    calendar()

    $(document).on("submit", "#car_wizards-form", function (e) {
        e.preventDefault()

        var form = $(this);
        var url = $(this).attr('action');
        e.preventDefault();


        var fileinput = $('.file-input-d').attr('id')

        var files = $("#" + fileinput).data('fileinput').fileManager.stack


        var dates = {available: [], unavailable: []}

        $('.owner_calendar_period').each(function (e) {
            var thisa = $(this)
            var period = thisa.attr('data-period')
            if (typeof period != 'undefined') {
                if (thisa.hasClass('available')) {
                    dates.available.push(period)
                } else {
                    dates.unavailable.push(period)
                }
            }
        })


        if (!$("#" + fileinput)[0].files.length) {
            // alertifyError(gettext('Please choose image files'))
            // return false
        }

        var formData = new FormData(this);
        formData.append('dates', JSON.stringify(dates))
        jQuery.each(files, function (i, file) {
            formData.append('file-' + i, files[i]['file']);
        });

        formData.append('displayed_months', $('.js_car_calendar').attr('data-months'));

        $.ajax({
            method: "post",
            url: url,
            dataType: "json",
            data: formData,
            mimeTypes: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {

                if (data.success == false) {
                    $.each(data.errors, function (index, value) {
                        $.each(value, function (index, value) {
                            var txt = '<li class="label-field">' + index + '<ul class="errorlist">';

                            $.each(value, function (index2, value2) {
                                txt += '<li>' + value2 + '</li>';
                            });
                            txt += '</ul></li>'
                            alertifyTogether(txt, "danger")
                        });
                    });
                } else if (data.success == true) {
                    // $('#car_wizards-form').trigger("reset");
                    // $("#"+fileinput).fileinput('reset')
                    window.location.href = data.redirect
                }

            }
        }), !1

    })

    jQuery('#calculation-form').submit(function (e) {
        if ($(this).hasClass('not_redirect')) {
            e.preventDefault()
        }
    });

    function afterAjax() {
        $(document).on('submit', '.cobalt-Button--fullWidth', function (e) {
            $('.drivy_open').val($(this).attr('data-set'))
        });
    }

    afterAjax()

    function alertifySuccess(message, modal = null, delay = 10) {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            $('#' + modal).modal('hide');
        }
    }

    function alertifyError(message, modal = null, delay = 10) {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);

        if (modal) {
            $('#' + modal).modal('hide');
        }
    }

    var dateNow = new Date()
    var endDate = new Date(dateNow.setMonth(dateNow.getMonth() + 1));

    if ($('#calendar-booking').length) {
        $('#calendar-booking').daterangepicker({
            opens: 'right',
            showDropdowns: true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "autoApply": true,
            "minDate": new Date(),
            "maxDate": endDate,
            onClose: function (dateText) {
                $(this).trigger('blur');
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    }

    var dateNow = new Date()
    var endDate = new Date(dateNow.setMonth(dateNow.getMonth() + 1));

    if ($('#start-date').length) {
        var datamax = $('#end-date').attr('data-max')
        $('#start-date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            "minDate": new Date(),
            "maxDate": datamax,
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function (start, end, label) {
        });
    }

    if ($('#end-date').length) {
        var datamax = $('#end-date').attr('data-max')
        var datamin = $('#start-date').attr('data-min')
        $('#end-date').daterangepicker({
            singleDatePicker: true,
            "maxDate": datamax,
            "minDate": datamin,
            showDropdowns: true,
        }, function (start, end, label) {
        });
    }

    if ($('.rental_duration').length) {
        var from = $(".rental_duration").attr('from')
        var to = $(".rental_duration").attr('to')

        $(".rental_duration").ionRangeSlider({
            type: "double",
            min: 0,
            max: 30,
            from: from ? from : 0,
            to: to ? to : 30,
            grid: true,
            onChange: function (data) {
                if (data.from != 0) {
                    $('.rental_duration_no_minimum').text(gettext('No rentals of ' + data.from + ' hours or less\n'))
                } else {
                    $('.rental_duration_no_minimum').text(gettext('No minimum'))
                }
            },
            onFinish: function (data) {
                $('.rental_duration_maximum_days').text(gettext('Maximum ' + data.to + ' days'))
            },
        });
    }

    if ($('.maximum_booking_notice').length) {
        var custom_values2 = ['The next 15 days', 'The next month', 'The next 3 months', 'The next 6 months', 'All future dates']
        var from = $(".maximum_booking_notice").attr('to')
        $(".maximum_booking_notice").ionRangeSlider({
            grid: true,
            from: from ? custom_values2.indexOf(from) : 4,
            values: custom_values2,
        });
    }

    if ($('[data-toggle="tooltip"]').length) {
        $('[data-toggle="tooltip"]').tooltip()
    }
});


function alertifyTogether(message, className, delay = 20) {
    if (className == 'success' || className == 'info') {
        var msg = alertify.success('Default message');
        msg.delay(10).setContent(message);
    } else {
        var msg = alertify.warning('Default message');
        msg.delay(10).setContent(message);
    }

}