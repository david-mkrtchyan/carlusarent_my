function fixHeaderHomepage() {
    $('.mobilefixed').each(function () {
        $(this).attr({'data-top': ($(this).offset().top - $(this).height())})
    });
    if ($(window).width() < 736) {
        $(window).scroll(function () {
            var windowPosition = $(this).scrollTop();
            var currentOffset = 0;
            $('.mobilefixed').each(function () {
                var offset = $(this).offset();
                var current = $('.mobilefixed.fixtop');
                if (windowPosition >= (offset.top - $(this).height())) {
                    current.removeClass('fixtop');
                    $(this).addClass('fixtop');
                }

            });
            if ($('.mobilefixed.fixtop').length > 0 && windowPosition < $('.mobilefixed.fixtop').data('top')) {
                $('.mobilefixed.fixtop').removeClass('fixtop');
            }
            if (windowPosition + 100 >= $('#aboutfazwaz').offset().top) {
                $('.mobilefixed.fixtop').removeClass('fixtop');
            }
        });
    }
}

$(document).ready(function () {

    $('.owl-carousel').owlCarousel({
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [767, 2],
        navigationText: ["<img src='media/left-arrow.png' alt=''>", "<img src='media/right-arrow.png' alt=''>"],
        itemsMobile: [479, 1],
        itemsMobileSmall: false,
        lazyLoad: true,
        margin: 10,
        nav: false,
        loop: false,
        pagination: false,

        navigation: true
    });
    fixHeaderHomepage();

});
$(document).resize(function () {
    fixHeaderHomepage();

});


jQuery(document).ready(function ($) {
    $('#wp-admin-bar-my-sites-search.hide-if-no-js').show();
    $('#wp-admin-bar-my-sites-search input').keyup(function () {

        var searchValRegex = new RegExp($(this).val(), 'i');

        $('#wp-admin-bar-my-sites-list > li.menupop').hide().filter(function () {

            return searchValRegex.test($(this).find('> a').text());

        }).show();

    });
});
