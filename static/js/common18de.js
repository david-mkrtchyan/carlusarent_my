/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

(function ($) {
  const videoSrc = $('#videoModal').find('iframe').attr('data-src');

  $('#videoModal').on('show.bs.modal', function () {
    // on opening the modal
    // set the video to autostart
    $('#videoModal').find('iframe').attr('src', videoSrc + '&amp;autoplay=1');
  });

  $('#videoModal').on('hidden.bs.modal', function () {
    // on closing the modal
    // stop the video
    $('#videoModal').find('iframe').attr('src', null);
  });

  $('ul.dropdown-menu a[href^="#divider"]')
    .hide()
    .after('<li class="divider"></li>');

  $('.go-to-top').click(function () {
    $('html,body').animate({
        scrollTop: 0
      },
      'slow'
    );
  });

  $('#lang-select').change(function () {
    // set the window's location property to the value of the option the user has selected
    window.location = $(this).val();
  });

  if ($('.park-agency').length) {    // element exists
    const comments = $('.agency-reviews').find('.row.mb-4');
    comments.remove();
    let total = comments.length;
    let count = 0;
    for (let comment of comments) {
      count = count + 1;
      if (count <= 2) {
        $('.agency-reviews').append(comment);
      }
      if (count > 2) {
        if (!$('.agency-reviews.show-more-hidden').length) {
          $('.agency-reviews').append('<div class="show-more-hidden"></div>');
          $('.agency-reviews .show-more-hidden').hide();
        }
        $('.agency-reviews .show-more-hidden').append(comment);
      }
    }
    if (total > 2) {
      $('.agency-reviews').append('<a style="cursor: pointer;" class="fa fa-plus show-more-comment"/>');
    }
    $('.show-more-comment').click(event => {
      event.preventDefault();
      $('.show-more-comment').toggleClass('fa-plus').toggleClass('fa-minus');
      $('.agency-reviews .show-more-hidden').toggle();
    });
  }

})(jQuery);
