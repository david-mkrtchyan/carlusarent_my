(function () {
    function r(e, n, t) {
        function o(i, f) {
            if (!n[i]) {
                if (!e[i]) {
                    var c = "function" == typeof require && require;
                    if (!f && c) return c(i, !0);
                    if (u) return u(i, !0);
                    var a = new Error("Cannot find module '" + i + "'");
                    throw a.code = "MODULE_NOT_FOUND", a
                }
                var p = n[i] = {exports: {}};
                e[i][0].call(p.exports, function (r) {
                    var n = e[i][1][r];
                    return o(n || r)
                }, p, p.exports, r, e, n, t)
            }
            return n[i].exports
        }

        for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) o(t[i]);
        return o
    }

    return r
})()({
    1: [function (require, module, exports) {
        module.exports = angular.module("auth.controllers", []).service("redirectTo", ["$window", "$stateParams", function ($window, $stateParams) {
            return function () {
                try {
                    $window.location.href = $window.location.protocol + "//" + $window.location.hostname + ($stateParams.redirectTo || "/")
                } catch (err) {
                    $window.location.href = "/"
                }
            }
        }]).controller("LoginCtrl", ["$scope", "$window", "$translate", "Auth", "$stateParams", "Config", "redirectTo", function ($scope, $window, $translate, Auth, $stateParams, Config, redirectTo) {
            $scope.$stateParams = $stateParams;
            $scope.Config = Config;
            $scope.auth = new Auth;
            $scope.submit = function () {
                $scope.auth.$login().then(function success() {
                    redirectTo()
                }).catch(function error(response) {
                    $window.alert($translate.instant(response.data.message))
                })
            };
            if ($scope.$stateParams.email && $scope.$stateParams.password) {
                $scope.auth.email = $scope.$stateParams.email;
                $scope.auth.password = $scope.$stateParams.password;
                $scope.submit()
            }
        }]).controller("ForgotPasswordCtrl", ["$scope", "Auth", "$window", "$translate", "AlertBox", function ($scope, Auth, $window, $translate, AlertBox) {
            $scope.auth = new Auth;
            $scope.submit = function () {
                $scope.auth.$forgotPassword().then(function () {
                    AlertBox.success($translate.instant("forgotPassword.success")).then(function () {
                        $window.location.href = "/"
                    })
                }).catch(function () {
                    $window.alert($translate.instant("forgotPassword.error"))
                })
            }
        }]).controller("ResetPasswordCtrl", ["$scope", "auth", "$state", "$window", "$translate", "AlertBox", function ($scope, auth, $state, $window, $translate, AlertBox) {
            $scope.auth = auth;
            $scope.submit = function () {
                $scope.auth.$resetPassword().then(function () {
                    AlertBox.success($translate.instant("resetPassword.success")).then(function () {
                        $state.go("login")
                    })
                }).catch(function () {
                    $window.alert($translate.instant("resetPassword.error"))
                })
            }
        }]).controller("SignUpCtrl", ["$scope", "$window", "Auth", "$translate", "_", "Config", "$stateParams", "redirectTo", function ($scope, $window, Auth, $translate, _, Config, $stateParams, redirectTo) {
            $scope.Config = Config;
            $scope.$stateParams = $stateParams;
            $scope.auth = new Auth;
            $scope.submit = function () {
                $scope.auth.$signUp().then(function () {
                    redirectTo()
                }).catch(function (response) {
                    $window.alert($translate.instant(response.data.message))
                })
            }
        }]).controller("LogoutCtrl", ["Auth", "$window", "$translate", function (Auth, $window, $translate) {
            Auth.logout().$promise.then(function () {
                $window.location.href = "/"
            }).catch(function () {
                $window.alert($translate.instant("logout.error"))
            })
        }])
    }, {}], 2: [function (require, module, exports) {
        module.exports = angular.module("auth.directives", []).directive("mustMatchWith", function () {
            return {
                restrict: "A",
                require: "ngModel",
                scope: {mustMatchWith: "="},
                link: function (scope, $element, attrs, ngModel) {
                    if (!ngModel) return;
                    scope.$watchGroup([function () {
                        return ngModel.$modelValue
                    }, "mustMatchWith"], function (newValue, oldValue) {
                        ngModel.$setValidity("match", newValue[0] === newValue[1])
                    }, true)
                }
            }
        }).directive("sexyPlaceholder", ["$timeout", function ($timeout) {
            var CLASS_NAME = "input-focused";
            return {
                require: "ngModel", link: function (scope, element, attr) {
                    $timeout(function () {
                        if (element[0].value.length > 0) {
                            element.addClass(CLASS_NAME)
                        }
                    });
                    element.bind("focus", function () {
                        element.addClass(CLASS_NAME)
                    });
                    element.bind("blur", function () {
                        if (element[0].value.length === 0) {
                            element.removeClass(CLASS_NAME)
                        }
                    })
                }
            }
        }])
    }, {}], 3: [function (require, module, exports) {
        module.exports = angular.module("auth", [require("./services").name, require("./directives").name, require("./controllers").name]).config(["$stateProvider", function ($stateProvider) {
            $stateProvider.state("sign-up", {
                url: "/sign-up?redirectTo",
                templateUrl: "templates/sign-up.html",
                controller: "SignUpCtrl"
            }).state("login", {
                url: "/login?redirectTo&email&password",
                templateUrl: "templates/login.html",
                controller: "LoginCtrl"
            }).state("forgot-password", {
                url: "/forgot-password",
                templateUrl: "templates/forgot-password.html",
                controller: "ForgotPasswordCtrl"
            }).state("reset-password", {
                url: "/reset-password/:_id?key",
                templateUrl: "templates/reset-password.html",
                controller: "ResetPasswordCtrl",
                resolve: {
                    auth: ["$stateParams", "Auth", "_", function ($stateParams, Auth, _) {
                        return Auth.getPasswordResetRequest(_.pick($stateParams, "_id", "key")).$promise
                    }]
                }
            }).state("logout", {url: "/logout", template: "", controller: "LogoutCtrl"})
        }])
    }, {"./controllers": 1, "./directives": 2, "./services": 4}], 4: [function (require, module, exports) {
        module.exports = angular.module("auth.services", []).factory("Auth", ["$resource", function ($resource) {
            return $resource("", {_id: "@_id"}, {
                login: {url: "../api/auth/login", method: "POST"},
                signUp: {url: "../api/auth/sign-up", method: "POST"},
                logout: {url: "../api/auth/logout", method: "POST"},
                forgotPassword: {url: "../api/auth/forgot-password", method: "POST"},
                resetPassword: {url: "../api/auth/reset-password/:_id", method: "PUT"},
                getPasswordResetRequest: {
                    url: "../api/auth/password-reset-requests/:_id",
                    method: "GET",
                    isArray: false
                }
            })
        }])
    }, {}], 5: [function (require, module, exports) {
        module.exports = angular.module("app-auth", ["auth-vendor", require("./set-config").name, require("./auth").name]).config(["$locationProvider", "$windowProvider", function ($locationProvider, $windowProvider) {
            var window = $windowProvider.$get();
            if (window.history && window.history.pushState) {
                $locationProvider.html5Mode(true)
            } else {
                window.location.hash = "/"
            }
        }]).config(["$uiViewScrollProvider", function ($uiViewScrollProvider) {
            $uiViewScrollProvider.useAnchorScroll()
        }]).run(["fastClick", function (fastClick) {
            if ("addEventListener" in window.document) {
                window.document.addEventListener("DOMContentLoaded", function () {
                    fastClick.attach(window.document.body)
                }, false)
            }
        }]).run(["$location", "$transitions", "_", "$state", function ($location, $transitions, _, $state) {
            $transitions.onStart({to: "**"}, function () {
                this.locationSearch = _.omit($location.search(), _.keys($state.params))
            });
            $transitions.onSuccess({to: "**"}, function () {
                $location.search(_.merge($location.search(), this.locationSearch))
            })
        }])
    }, {"./auth": 3, "./set-config": 6}], 6: [function (require, module, exports) {
        module.exports = angular.module("set-config", []).config(["Config", "GoogleTagManagerProvider", function (Config, GoogleTagManagerProvider) {
            GoogleTagManagerProvider.setContainerID(Config.googleTagManager.containerID)
        }]).config(["Config", "PhoneUtilsProvider", function (Config, PhoneUtilsProvider) {
            PhoneUtilsProvider.setDefaultCountry(Config.country)
        }])
    }, {}]
}, {}, [5]);