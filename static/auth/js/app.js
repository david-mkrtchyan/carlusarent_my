$("input").focusout(function () {
    if (!$(this).val()) {
        $(this).removeClass("input-focused");
    }
})
    .focus(function () {
        $(this).addClass("input-focused");
    });

$("input").each(function (e) {
    if ($(this).val()) {
        $(this).addClass("input-focused");
    }
})

