from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation
)
from .models import Users as User, Verify, DriverLicense
from django.utils.text import capfirst
from django.utils.translation import gettext, gettext_lazy as _
from intl_tel_input.widgets import IntlTelInputWidget
from django.template import loader
from django.core.mail import send_mail
from django.utils.translation import gettext as _
from django.contrib import messages
from models.models import Images
from PIL import Image
from django_countries.fields import CountryField
from django.forms import DateField

UserModel = get_user_model()


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'works', 'languages', 'school', 'live', 'about',
                  'mobile_notice', 'email_notice', 'transmission']


class VerifyForm(forms.ModelForm):
    email = forms.EmailField(
        label=_("Email"), required=True
    )

    class Meta:
        model = Verify
        fields = ['type', 'token', 'value', 'user']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Images
        fields = ['path']


class ApproveDriveViewForm(forms.ModelForm):
    issuing_country = CountryField(max_length=255)
    current_country = CountryField(max_length=255)
    date_of_issuance = DateField()

    class Meta:
        model = DriverLicense
        fields = ['user', 'license_number', 'issuing_country', 'date_of_issuance', 'first_name', 'last_name',
                  'middle_name',
                  'date_of_birth', 'current_country',
                  'current_street_address', 'current_city', 'current_state_province_reqion', 'current_postal_code']
        exclude = ("user",)


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        required=True,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )

    terms = forms.BooleanField(
        label=_("Terms"), required=True
    )

    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        required=True,
        help_text=_("Enter the same password as before, for verification."),
    )

    first_name = forms.CharField(
        label=_("First Name"),
        min_length=3,
        required=True
    )

    last_name = forms.CharField(
        label=_("Last Name"),
        min_length=3,
        required=True
    )

    phone_number = forms.CharField(label='Phone Number', max_length=45, required=True)

    email = forms.EmailField(label=_('Email Address'), required=True)

    validate_phone = forms.BooleanField(label=_('Phone Number'), required=False)

    class Meta:
        model = User
        fields = ("email", "password1", 'validate_phone', 'password2', 'first_name', 'last_name', 'phone_number')
        field_classes = {}
        widgets = {
            'phone_number': IntlTelInputWidget()
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self._meta.model.EMAIL_FIELD in self.fields:
            self.fields[self._meta.model.EMAIL_FIELD].widget.attrs.update({'autofocus': True})

    def clean_validate_phone(self):
        validate_phone = self.cleaned_data.get("validate_phone")
        if validate_phone == None or validate_phone == False:
            raise forms.ValidationError(
                'Phone number is wrong',
            )
        return validate_phone

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        from django.utils.crypto import get_random_string
        username = get_random_string(length=32)
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.username = username
        user.is_active = False
        if commit:
            from carlusarent.settings import SITE_URL, EMAIL_HOST_USER
            to_list = [user.email]
            subject = _(f"Thank you for register to {SITE_URL}")

            from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
            from django.template.loader import render_to_string
            from django.utils.encoding import force_bytes, force_text
            from .tokens import account_activation_token

            # https://medium.com/@frfahim/django-registration-with-confirmation-email-bb5da011e4ef
            # html_message = loader.render_to_string(
            #     'mail/register.html',
            #     {
            #         'user_name': user.first_name,
            #     }
            # )

            user.save()

            message = render_to_string('mail/confirmation.html', {
                'user': user,
                'domain': SITE_URL,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),  # .decode()
                'token': account_activation_token.make_token(user),
            })
            send_mail(subject, message, EMAIL_HOST_USER, to_list, fail_silently=True, html_message=message)

        return user


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    email/password logins.
    """
    email = forms.EmailField(label=_("Email"), required=True)
    password = forms.CharField(
        label=_("Password"),
        required=True,
        strip=False,
        widget=forms.PasswordInput,
    )

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(email)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the max length and label for the "email" field.
        self.email_field = UserModel._meta.get_field(UserModel.EMAIL_FIELD)
        self.fields['email'].max_length = self.email_field.max_length or 254
        if self.fields['email'].label is None:
            self.fields['email'].label = capfirst(self.email_field.verbose_name)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email is not None and password:

            username = self.get_user2(email.lower())

            self.user_cache = authenticate(self.request, username=username, password=password)

            if self.user_cache is None:
                for mess in self.get_invalid_login_error():
                    messages.error(self.request, _(mess))

                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    @staticmethod
    def get_user2(email):
        try:
            return User.objects.filter(email=email).first()
        except User.DoesNotExist:
            return None

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={'email': self.email_field.verbose_name},
        )
