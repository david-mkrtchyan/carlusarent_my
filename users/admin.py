from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.

from .models import Users
from django.utils.translation import gettext_lazy as _


class UserAdminCustom(UserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff', 'phone_number', 'live', 'languages', 'works',
        'school', 'about', 'mobile_notice', 'email_notice', 'transmission')

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'), {'fields': (
                'first_name', 'last_name', 'email', 'phone_number')}),
        (_('Additional info'),
         {'fields': ('languages', 'live', 'works', 'school', 'about', 'mobile_notice', 'email_notice',
                     'transmission', 'balance', 'approved_to_drive',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(Users, UserAdminCustom)
