from django.shortcuts import render, redirect
from django.contrib.auth import views as auth_views
from django.http import HttpResponseRedirect
from django.utils.translation import gettext as _
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from .forms import AuthenticationForm, UserCreationForm, UserUpdateForm, ProfileUpdateForm, VerifyForm, \
    ApproveDriveViewForm
from django.views.generic.edit import FormView
from django.shortcuts import resolve_url
from carlusarent.settings import LOGIN_REDIRECT_URL
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from .tokens import account_activation_token
from .models import Users as User, type_email, DriverLicense
from django.urls import reverse_lazy
from models.models import type_profile
from uuid import uuid4
from datetime import datetime
from users.models import Verify
from django.utils.decorators import method_decorator
from decorators.own_decorator import checkLoggedInAndRequestUserSame


class Login(auth_views.LoginView):
    form_class = AuthenticationForm

    def form_valid(self, form):
        login(self.request, form.get_user())

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'], = str(_('Sign In')),
        context['breadcrumb'], = str(_('Sign In')),

        return context


class PasswordResetConfirm(auth_views.PasswordResetConfirmView):
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        messages.success(self.request, _(
            'Your passwprd has been set'))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'], = str(_('Password Reset Confirm')),
        context['breadcrumb'], = str(_('Password Reset Confirm')),

        return context


class PasswordReset(auth_views.PasswordResetView):
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
        form.save(**opts)

        email = User.objects.filter(email=form.cleaned_data.get("email")).first()

        if email == None:
            messages.error(self.request, _('An email is wrong'))
            return render(self.request, self.template_name, {'form': form})
        else:
            messages.success(self.request, _(
                'An email has been send with instructions to reset your password'))

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'], = str(_('Password Reset')),
        context['breadcrumb'], = str(_('Password Reset')),

        return context


class PasswordChange(auth_views.PasswordChangeView):
    success_url = reverse_lazy('profile')


class UserRegister(FormView):
    form_class = UserCreationForm

    initial = {'key': 'value'}
    template_name = 'users/register.html'

    def get_success_url(self):
        return resolve_url(LOGIN_REDIRECT_URL)

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()

            messages.success(self.request, _(
                'Your account has been created! for activate that go to your email and follow instructions'))

            return HttpResponseRedirect(self.get_success_url())

        return render(request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'], = str(_('Register')),
        context['breadcrumb'], = str(_('Register')),

        return context


class ApproveDriveView(FormView):
    form_class = ApproveDriveViewForm

    template_name = "users/approve-drive.html"

    @method_decorator(login_required)
    @method_decorator(checkLoggedInAndRequestUserSame)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):

        if form_class is None:
            form_class = self.form_class

        try:
            driver = DriverLicense.objects.filter(user=self.request.user).first()
            return self.form_class(instance=driver, **self.get_form_kwargs())
        except:
            return self.form_class(**self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            new_data = form.save(commit=False)
            new_data.user = self.request.user
            new_data.save()

            if not self.request.user.approved_to_drive:
                self.request.user.approved_to_drive = True
                self.request.user.save()
                messages.success(self.request, _('Your request to get an approval for a drive is accepted!'))

            return HttpResponseRedirect(resolve_url('account', self.request.user.id))

        return render(request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        from time import gmtime, strftime

        context = super().get_context_data(**kwargs)
        context['date_now'], = strftime("%Y-%m-%d", gmtime()),
        context['title'], = str(_('Approved to drive')),
        context['breadcrumb'], = str(_('Approved to drive')),

        return context


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        messages.success(request, _(
            'Thank you for your email confirmation. Now you can login your account.'))
        return HttpResponseRedirect('/')
    else:
        messages.success(request, _(
            'Activation link is invalid!'))
        return HttpResponseRedirect('/')


@login_required()
def verifyEmail(request, token):
    result = Verify.objects.filter(token=token).first()
    if result:
        result.verified = True
        result.save()

        user = result.user
        user.email = result.value
        user.save()

        messages.success(request, _(
            'Your email has been verified'))
    else:
        messages.success(request, _(
            'Something was wrong'))

    return redirect('account', request.user.id)


def profileView(request, id):
    userData = User.objects.filter(pk=id).first()
    if not userData:
        from django.http import HttpResponseNotFound
        return HttpResponseNotFound()

    return render(request, 'users/profile-view.html', {
        'userData': userData
    })


@login_required
@checkLoggedInAndRequestUserSame
def account(request, id):
    from car.models import PriceCalculation

    if request.method == 'POST':

        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES)
        verify = VerifyForm(request.POST)

        if verify.is_valid():

            email = verify.cleaned_data["email"].replace(" ", "")
            lastemail = request.user.email.replace(" ", "")

            if email and email != lastemail:

                verifyData = Verify.objects.filter(value=email, user=request.user).first()
                if not verifyData:
                    verifyData = Verify.objects.create(
                        user=request.user, type=type_email, token=uuid4(), value=email, created_at=datetime.now(),
                        updated_at=datetime.now()
                    )
                    verify.save(commit=False)
                    verifyData.user = request.user
                    verifyData.save()
                else:
                    verifyData.token = uuid4()
                    verifyData.save()

                messages.success(request, _(
                    'Your mentioned email address have been sent verification email, go to your email and follow instructions'))

                from django.core.mail import send_mail
                from django.template.loader import render_to_string
                from .tokens import account_activation_token
                from carlusarent.settings import SITE_URL, EMAIL_HOST_USER

                message = render_to_string('mail/email_confirmation.html', {
                    'user': request.user,
                    'email': email,
                    'domain': SITE_URL,
                    'token': verifyData.token,
                })

                send_mail(_('Please verify your email address'), message, EMAIL_HOST_USER, [email],
                          fail_silently=True, html_message=message)

        if u_form.is_valid():

            u_form.save()

            if p_form.is_valid():
                form = p_form.save(commit=False)
                form.object_id = request.user.pk
                form.type = type_profile
                form.save()

            messages.success(request, _('Profile info updated successfully'))

        return redirect('account', request.user.id)
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm()
        verify = VerifyForm()

    context = {
        'p_form': p_form,
        'verify': verify,
        'u_form': u_form,
        'title': _('Update your account and profile'),
        'breadcrumb': _('Account Profile'),
    }
    return render(request, 'users/account.html', context)
