# Generated by Django 2.2.2 on 2019-07-21 19:36

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0058_auto_20190719_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 21, 19, 36, 5, 440736, tzinfo=utc)),
        ),
    ]
