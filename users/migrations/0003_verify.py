# Generated by Django 2.2.2 on 2019-06-28 21:06

import datetime
from django.conf import settings
from django.db import migrations, models
import django.utils.timezone
from django.utils.timezone import utc
import django_mysql.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20190626_2251'),
    ]

    operations = [
        migrations.CreateModel(
            name='Verify',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', django_mysql.models.EnumField(choices=[('email', 'email'), ('phone', 'phone')], default='email')),
                ('token', models.CharField(max_length=255)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(default=datetime.datetime(2019, 6, 28, 21, 6, 8, 33231, tzinfo=utc), verbose_name=django.utils.timezone.now)),
                ('user', models.ForeignKey(on_delete=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
