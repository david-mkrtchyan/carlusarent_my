# Generated by Django 2.2.2 on 2019-07-01 16:18

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20190701_2006'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='about',
            field=models.TextField(default=None, verbose_name='About'),
        ),
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 1, 16, 18, 27, 57318, tzinfo=utc)),
        ),
    ]
