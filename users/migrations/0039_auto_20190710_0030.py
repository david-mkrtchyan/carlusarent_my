# Generated by Django 2.2.2 on 2019-07-09 20:30

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0038_auto_20190710_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 9, 20, 30, 24, 723900, tzinfo=utc)),
        ),
    ]
