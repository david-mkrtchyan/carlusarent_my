# Generated by Django 2.2.2 on 2019-07-08 18:57

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0029_auto_20190706_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 8, 18, 57, 13, 18156, tzinfo=utc)),
        ),
    ]
