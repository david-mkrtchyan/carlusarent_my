# Generated by Django 2.2.2 on 2019-07-09 20:26

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0035_auto_20190710_0002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 9, 20, 26, 27, 542282, tzinfo=utc)),
        ),
    ]
