# Generated by Django 2.2.2 on 2019-07-13 12:29

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0047_auto_20190713_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verify',
            name='updated_at',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 7, 13, 12, 29, 57, 817907, tzinfo=utc)),
        ),
    ]
