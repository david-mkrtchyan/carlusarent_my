from django.urls import path
from . import views
from users import views as users_views
from django.contrib.auth import views as auth_views
from django.utils.translation import gettext as _
from django.conf.urls import url

urlpatterns = [

    path('register/', views.UserRegister.as_view(
        extra_context={'title': str(_('Register User')), 'breadcrumb': str(_('Register User'))},
        template_name='users/register.html'), name='register'),

    path('password-change/', views.PasswordChange.as_view(
        extra_context={'title': str(_('Password Change')), 'breadcrumb': str(_('Password Change'))},
        template_name='users/password-change.html'), name='password-change'),

    path('login/',
         views.Login.as_view(extra_context={'title': str(_('Sign In')), 'breadcrumb': str(_('Sign In'))},
                             template_name='users/login.html'),
         name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),

    path('password-reset/', views.PasswordReset.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset.html'), name='password_reset'),

    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', views.PasswordResetConfirm.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        extra_context={'title': str(_('Reset password')), 'breadcrumb': str(_('Reset password'))},
        template_name='users/password_reset_complete.html'), name='password_reset_complete'),

    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

    path('account/<id>', views.account, name='account'),
    path('approve-drive/<id>', views.ApproveDriveView.as_view(
        extra_context={'title': str(_('Register User')), 'breadcrumb': str(_('Register User'))},
        template_name='users/approve-drive.html'), name='approve-drive'),
    path('profile/view/<id>', views.profileView, name='profile-view'),
    path('verify-email/<token>/', views.verifyEmail, name='verify-email'),

]
