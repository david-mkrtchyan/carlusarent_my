from django.db import models
from django.contrib.auth.models import AbstractUser
from models.models import Images, type_profile
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django_mysql.models import EnumField
from django_countries.fields import CountryField


# Create your models here.

class Users(AbstractUser):
    # add another user columns
    phone_number = models.CharField(max_length=45, null=True, blank=True)

    first_name = models.CharField(_('First name'), max_length=30, null=True, blank=True)
    last_name = models.CharField(_('Last name'), max_length=150, null=True, blank=True)
    live = models.CharField(_('Live'), max_length=150, default=None, null=True, blank=True)
    languages = models.CharField(_('Live'), max_length=250, default=None, null=True, blank=True)
    works = models.CharField(_('Works'), max_length=250, default=None, null=True, blank=True)
    school = models.CharField(_('School'), max_length=250, default=None, null=True, blank=True)
    about = models.TextField(_('About'), default=None, null=True, blank=True)
    mobile_notice = models.BooleanField(_('Mobile Notice'), default=True, null=True, blank=True)
    email_notice = models.BooleanField(_('Email Notice'), default=True, null=True, blank=True)
    transmission = models.BooleanField(_('Transmission'), default=False, null=True, blank=True)
    balance = models.DecimalField(_('Balance'), max_digits=10, decimal_places=10, null=True, blank=True)
    approved_to_drive = models.BooleanField(_('Approved to drive'), default=False, null=True, blank=True)

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'users'

    def profile(self):
        return Images.objects.filter(type=type_profile, object_id=self.pk).order_by('-id').first()

    def driverLicense(self):
        return DriverLicense.objects.filter(user_id=self.pk).first()


class DriverLicense(models.Model):
    user = models.ForeignKey(Users, on_delete=True)
    license_number = models.CharField(max_length=255)
    issuing_country = models.CharField(max_length=255)
    date_of_issuance = models.DateField(max_length=255)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    date_of_birth = models.DateTimeField()
    current_country = models.CharField(max_length=255)
    current_street_address = models.CharField(max_length=255)
    current_city = models.CharField(max_length=255)
    current_state_province_reqion = models.CharField(max_length=255)
    current_postal_code = models.CharField(max_length=255)

    class Meta:
        db_table = 'user_driver_license'


type_email = 'email'
type_phone = 'phone'


class Verify(models.Model):
    user = models.ForeignKey(Users, on_delete=True, blank=True)
    type = EnumField(choices=[type_email, type_phone], default=('email'), blank=True)
    token = models.CharField(max_length=255, blank=True)
    value = models.CharField(max_length=255, default=None, blank=True)
    verified = models.BooleanField(default=False, blank=True)
    created_at = models.DateField(auto_now_add=True, blank=True)
    updated_at = models.DateField(default=timezone.now(), blank=True)

    class Meta:
        db_table = 'user_verify'
