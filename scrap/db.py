import sqlite3

location = 'data.db'

import mysql.connector


class Database:

    def __init__(self):
        global conn
        global c

        conn = mysql.connector.connect(
            host="localhost",
            user="root",
                database="carlusarent",
            passwd=""
        )

        c = conn.cursor()

    def create_database(self, sql):
        c.execute(sql)
        conn.commit()

    def drop(self, table_name):
        c.execute('drop table ' + table_name)
        conn.commit()

    def insert_or_update_record(self, sql, tuple):
        c.execute(sql, tuple)
        conn.commit()
        c.execute('SELECT last_insert_id()')
        return c.fetchone()

    def get(self, table_name, id, conditionField='id'):
        c.execute(f'''SELECT *FROM {table_name} WHERE {conditionField}=%s''', (id,))
        return c.fetchone()

    def delete_task(self, id, table_name):
        sql = f'''DELETE FROM {table_name} WHERE id=?'''
        c.execute(sql, (id,))

    def delete_task_condition(self, table_name, condition):
        sql = f'''DELETE FROM {table_name} WHERE {condition}'''
        c.execute(sql)
        conn.commit()

    def delete_all_tasks(self, table_name):
        sql = f'''DELETE FROM {table_name}'''
        c.execute(sql)
        conn.commit()

    def select_all(self, table_name):
        c.execute(f'''SELECT *FROM {table_name}''')
        return c.fetchall()

    def select_all_condition(self, table_name, condition):
        c.execute(f'''SELECT *FROM {table_name} WHERE {condition} ''')
        return c.fetchall()

    def select_one_condition(self, table_name, condition):
        c.execute(f'''SELECT *FROM {table_name} WHERE {condition} ''')
        return c.fetchone()

    def select_one(self, table_name):
        c.execute(f'''SELECT *FROM {table_name}''')
        return c.fetchone()

    def structure(self, table_name):
        c.execute(f'''select name from sqlite_master where type = '{table_name}';''')
        return c.fetchall()


database = Database()
