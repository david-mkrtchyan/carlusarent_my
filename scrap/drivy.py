import requests as httprequest
from bs4 import BeautifulSoup
import urllib.request, json
import time
import socket
from scrap.db import database

urlDrivy = 'https://www.drivy.co.uk/rent-your-car'
brands_models = 'https://www.drivy.co.uk/car_models'
mileages = {
    1: '0-10,000 mi',
    2: '10-30,000 mi',
    3: '30-60,000 mi',
    4: '60-90,000 mi',
    5: '90-130,000 mi',
    6: '130,000 mi+',
}

import logging
logging.basicConfig(filename='C:/xampp7.3.6/htdocs/python/p/carlusarent/example.log', level=logging.DEBUG)

class DrivyScrap(object):

    def run(self):

        startYear = 2010
        yearNow = int(time.strftime("%Y")) + 1

        source = httprequest.get(urlDrivy).text
        soup = BeautifulSoup(source, 'lxml')

        selects = soup.find('select', id='car_model_estimation_car_brand_id')
        options = selects.find_all('option')

        import requests
        requests.urllib3.disable_warnings()
        for key, value in enumerate(options):
            if value and value['value']:
                if not value or not value.get('value') or value.get('value') == 'other':
                    continue

                brand_id = int(value['value'])
                data = requests.api.request('get', f'https://www.drivy.co.uk/car_models/models?make={brand_id}',
                                            data={},
                                            json=True, verify=False)
                if not data or not data.content:
                    continue

                datas = json.loads(data.content.decode())
                if not datas:
                   continue

                for data in datas:
                    brandModel = database.get('car_brands', value.text, 'name')

                    if not brandModel:
                        sql = '''INSERT INTO car_brands(name) VALUES(%s);'''
                        tuple = (value.text,)

                        brandModel = database.insert_or_update_record(sql, tuple)


                    data['localized_label'] = data['localized_label'].replace("'",'')
                    if not data or not data['localized_label']:
                        pass

                    modelModel = database.select_all_condition('car_models', f"name='{data['localized_label']}'")
                    if modelModel:
                       for mod in modelModel:
                           database.delete_task_condition('car_price_calculation', f"model_id='{mod[0]}'")
                           database.delete_task_condition('car_models', f"id='{mod[0]}'")

                       sql = '''INSERT INTO car_models(name,brand_id) VALUES(%s,%s);'''
                       tuple = (data['localized_label'],brandModel[0],)
                       modelModel= database.insert_or_update_record(sql, tuple)
                    else:
                        sql = '''INSERT INTO car_models(name,brand_id) VALUES(%s,%s);'''
                        tuple = (data['localized_label'],brandModel[0],)
                        modelModel= database.insert_or_update_record(sql, tuple)


                    model_id = data['id']

                    for i in range(startYear, yearNow):
                        for keyMi, mileage in mileages.items():

                             requestUri = f'https://www.drivy.co.uk/car_models/estimated_earnings?utf8=%E2%9C%93&car_model_estimation%5Bcar_brand_id%5D={brand_id}&car_model_estimation%5Bcar_model_id%5D={model_id}&car_model_estimation%5Brelease_year%5D={i}&car_model_estimation%5Bmileage%5D={keyMi}&car_model_estimation%5Bcity%5D=London&car_model_estimation%5Blatitude%5D=51.54546700880182&car_model_estimation%5Blongitude%5D=-0.21575852747134142&car_model_estimation%5Bregistration_country%5D=GB&car_model_estimation%5Bwith_open_landing_multiplier%5D=true'

                             data = requests.api.request('get', requestUri, data={}, json=True, verify=False)
                             if not data or not data.content:
                                 continue

                             datas = json.loads(data.content.decode())
                             if not datas:
                                 continue

                             soupPro = BeautifulSoup(datas['html'], 'lxml')
                             price = soupPro.find('span', class_='car_model_estimation_result_amount')

                             if price and price.text:
                                 priceCalc = database.select_all_condition('car_price_calculation',f"model_id='{modelModel[0]}' AND brand_id='{brandModel[0]}' AND millage='{keyMi}' AND year='{i}'")

                                 logging.debug(f'brand-{brandModel[0]} model-{modelModel[0]} millage-{keyMi} year-{i}')

                                 if priceCalc:
                                     for mod in priceCalc:
                                         database.delete_task_condition('car_price_calculation', f"id='{mod[0]}'")

                                     sql = '''INSERT INTO car_price_calculation(brand_id,model_id,millage,year,price) VALUES(%s,%s,%s,%s,%s);'''
                                     tuple = (brandModel['id'],modelModel['id'],keyMi,i,price.text,)
                                     database.insert_or_update_record(sql, tuple)

                                 else:
                                     sql = '''INSERT INTO car_price_calculation(brand_id,model_id,millage,year,price) VALUES(%s,%s,%s,%s,%s);'''
                                     tuple = (brandModel[0], modelModel[0], keyMi, i, price.text,)
                                     database.insert_or_update_record(sql, tuple)


print(DrivyScrap().run())